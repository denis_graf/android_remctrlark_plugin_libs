/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.rcajava_impl;

import com.github.rosjava.android_remctrlark.rcajava.IJavaRemoCompImpl;
import com.github.rosjava.android_remctrlark.rcajava.binders.IJavaRemoCompBinderImpl;
import com.github.rosjava.android_remctrlark.rcajava.configuration.IJavaRemoCompConfiguration;
import com.github.rosjava.android_remctrlark.rcajava.context.IJavaRemoCompContext;
import com.github.rosjava.android_remctrlark.rcajava.context.config.IJavaRemoCompConfig;
import com.github.rosjava.android_remctrlark.rcajava.context.layout.IJavaRemoCompLayout;
import com.github.rosjava.android_remctrlark.rcajava.context.misc.IJavaRemoCompEventDispatcher;

import java.lang.ref.WeakReference;

/**
 * This abstract base class is to be implemented in order to provide a Java RemoComp type. It provides a minimalistic
 * base implementation and some helper methods.
 */
public abstract class AJavaRemoCompImplBase extends AJavaRemoCoImplBase implements IJavaRemoCompImpl {
	private WeakReference<IJavaRemoCompContext> mRcaContext;

	@Override
	protected final IJavaRemoCompContext getRcaContext() {
		return mRcaContext.get();
	}

	@Override
	protected final IJavaRemoCompConfig getConfig() {
		return mRcaContext.get().getConfig();
	}

	@Override
	protected final IJavaRemoCompLayout getLayout() {
		return mRcaContext.get().getLayout();
	}

	/**
	 * @see com.github.rosjava.android_remctrlark.rcajava.context.IJavaRemoCompContext#getEventDispatcher()
	 */
	protected final IJavaRemoCompEventDispatcher getEventDispatcher() {
		return mRcaContext.get().getEventDispatcher();
	}

	@Override
	public IJavaRemoCompBinderImpl createBinder() throws Throwable {
		return null;
	}

	@Override
	public void init(IJavaRemoCompContext context) throws Throwable {
		mRcaContext = new WeakReference<IJavaRemoCompContext>(context);
		initDebug();
	}

	@Override
	public void start(IJavaRemoCompConfiguration configuration) throws Throwable {
	}
}
