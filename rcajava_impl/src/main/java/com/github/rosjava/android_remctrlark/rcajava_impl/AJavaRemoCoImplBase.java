/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.rcajava_impl;

import android.content.Context;
import android.view.ViewGroup;

import com.github.rosjava.android_remctrlark.rcajava.IJavaRemoCoImpl;
import com.github.rosjava.android_remctrlark.rcajava.context.IJavaRemoCoContext;
import com.github.rosjava.android_remctrlark.rcajava.context.config.IJavaRemoCoConfig;
import com.github.rosjava.android_remctrlark.rcajava.context.layout.IJavaRemoCoLayout;
import com.github.rosjava.android_remctrlark.rcajava.context.misc.IRemoCoLog;

import org.ros.node.NodeMainExecutor;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamClass;
import java.io.Serializable;

/**
 * This abstract base class is to be implemented in order to provide a Java RemoCo type. It provides a minimalistic base
 * implementation and some helper methods.
 */
abstract class AJavaRemoCoImplBase implements IJavaRemoCoImpl {

	/**
	 * RemoCo parameter identifier used by the RemoCo's debugging setting.
	 */
	protected static final String PARAM_NAME_DEBUG = "DEBUG";

	/**
	 * True if the debugging setting is activated in the RemoCo's configuration.
	 * This member variable is intended to be used by developers of RemoCos as a switch for activating debugging code,
	 * e.g., additional logging and run-time checks.
	 */
	protected boolean debug = false;

	/**
	 * Returns the RemoCo's context instance.
	 */
	protected abstract IJavaRemoCoContext getRcaContext();

	/**
	 * @see com.github.rosjava.android_remctrlark.rcajava.context.IJavaRemoCoContext#getConfig()
	 */
	protected abstract IJavaRemoCoConfig getConfig();

	/**
	 * @see com.github.rosjava.android_remctrlark.rcajava.context.IJavaRemoCoContext#getLayout()
	 */
	protected abstract IJavaRemoCoLayout getLayout();

	/**
	 * @see com.github.rosjava.android_remctrlark.rcajava.context.IJavaRemoCoContext#getLog()
	 */
	protected final IRemoCoLog getLog() {
		return getRcaContext().getLog();
	}

	/**
	 * @see com.github.rosjava.android_remctrlark.rcajava.context.IJavaRemoCoContext#getApkContext()
	 */
	protected final Context getApkContext() {
		return getRcaContext().getApkContext();
	}

	/**
	 * @see com.github.rosjava.android_remctrlark.rcajava.context.IJavaRemoCoContext#getLayout()#getParentView()
	 */
	protected final ViewGroup getParentView() {
		return getRcaContext().getLayout().getParentView();
	}

	/**
	 * @see com.github.rosjava.android_remctrlark.rcajava.context.IJavaRemoCoContext#getNodeExecutor()
	 */
	protected final NodeMainExecutor getNodeExecutor() {
		return getRcaContext().getNodeExecutor();
	}

	@Override
	public void createUI() throws Throwable {
	}

	@Override
	public void shutdown() throws Throwable {
	}

	/**
	 * Sets the debugging setting of this RemoCo by reading its configuration.
	 */
	protected void initDebug() {
		debug = getConfig().getParamBoolean(PARAM_NAME_DEBUG, debug);
	}

	/**
	 * Utility for restoring a state object retrieved by {@link IJavaRemoCoContext#getRemoConSessionState()}. This is
	 * useful when relaunching RemoCos after a dynamical RCA-Plugin reload, which causes a {@link ClassCastException}
	 * when trying to cast the stored state to the correct type. This utility method uses the serialization mechanism to
	 * workaround that issue.
	 *
	 * @param stateType Type of the state object to be restored.
	 * @param <T>       Type of the state object to be restored.
	 * @return Restored state, or {@code null} if there was no state stored or it could not be restored.
	 */
	protected <T extends Serializable> T restoreRemoConSessionState(final Class<T> stateType) {
		final Object state = getRcaContext().getRemoConSessionState();
		try {
			return stateType.cast(state);
		} catch (ClassCastException e1) {
			ByteArrayOutputStream baOut = null;
			ObjectOutputStream oOut = null;
			ByteArrayInputStream baIn = null;
			ObjectInputStream oIn = null;
			try {
				baOut = new ByteArrayOutputStream();
				oOut = new ObjectOutputStream(baOut);
				oOut.writeObject(state);
				baIn = new ByteArrayInputStream(baOut.toByteArray());
				oIn = new ObjectInputStream(baIn) {
					protected Class<?> resolveClass(ObjectStreamClass desc) throws IOException, ClassNotFoundException {
						return stateType.getClassLoader().loadClass(desc.getName());
					}
				};
				final T restoredState = stateType.cast(oIn.readObject());
				getRcaContext().setRemoConSessionState(restoredState);
				if (debug) {
					getLog().d("remocon session state successfully restored after ClassCastException");
				}
				return restoredState;
			} catch (Exception e2) {
				getLog().w("ignoring: remocon session state could not be restored: " +
						e2.toString() + "\n  caused by: " + e1.toString());
				return null;
			} finally {
				closeStream(oIn);
				closeStream(baIn);
				closeStream(oOut);
				closeStream(baOut);
			}
		}
	}

	private static void closeStream(Closeable baOut) {
		if (baOut != null) {
			try {
				baOut.close();
			} catch (IOException ignored) {
			}
		}
	}
}
