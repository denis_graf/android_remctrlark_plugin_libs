/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.rcajava_impl.error_handling;

public class RcaJavaImplRuntimeException extends RuntimeException {

	/**
	 * Constructs a new {@code RcaJavaImplRuntimeException} that includes the current stack
	 * trace.
	 */
	public RcaJavaImplRuntimeException() {
	}

	/**
	 * Constructs a new {@code RcaJavaImplRuntimeException} with the current stack trace
	 * and the specified detail message.
	 *
	 * @param message the detail message for this exception.
	 */
	public RcaJavaImplRuntimeException(String message) {
		super(message);
	}

	/**
	 * Constructs a new {@code RcaJavaImplRuntimeException} with the current stack trace,
	 * the specified detail message and the specified cause.
	 *
	 * @param message the detail message for this exception.
	 * @param cause     the cause of this exception.
	 */
	public RcaJavaImplRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructs a new {@code RcaJavaImplRuntimeException} with the current stack trace
	 * and the specified cause.
	 *
	 * @param cause the cause of this exception.
	 */
	public RcaJavaImplRuntimeException(Throwable cause) {
		super(cause);
	}
}
