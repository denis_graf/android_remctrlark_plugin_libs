/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.rcajava_impl;

import com.github.rosjava.android_remctrlark.rcajava.IJavaRemoConImpl;
import com.github.rosjava.android_remctrlark.rcajava.configuration.IJavaRemoConConfiguration;
import com.github.rosjava.android_remctrlark.rcajava.context.IJavaRemoConContext;
import com.github.rosjava.android_remctrlark.rcajava.context.config.IJavaRemoConConfig;
import com.github.rosjava.android_remctrlark.rcajava.context.layout.IJavaRemoConLayout;
import com.github.rosjava.android_remctrlark.rcajava.context.misc.IRemoConRelauncher;

import java.lang.ref.WeakReference;

/**
 * This abstract base class is to be implemented in order to provide a Java RemoCon type. It provides a minimalistic
 * base implementation and some helper methods.
 */
public abstract class AJavaRemoConImplBase extends AJavaRemoCoImplBase implements IJavaRemoConImpl {
	private WeakReference<IJavaRemoConContext> mRcaContext;

	/**
	 * Returns the RemoCon's context instance.
	 */
	@Override
	protected final IJavaRemoConContext getRcaContext() {
		return mRcaContext.get();
	}

	@Override
	protected final IJavaRemoConConfig getConfig() {
		return mRcaContext.get().getConfig();
	}

	@Override
	protected final IJavaRemoConLayout getLayout() {
		return mRcaContext.get().getLayout();
	}

	/**
	 * @see com.github.rosjava.android_remctrlark.rcajava.context.IJavaRemoConContext#getRelauncher()
	 */
	protected final IRemoConRelauncher getRelauncher() {
		return mRcaContext.get().getRelauncher();
	}

	@Override
	public void init(IJavaRemoConContext context) throws Throwable {
		mRcaContext = new WeakReference<IJavaRemoConContext>(context);
		initDebug();
	}

	@Override
	public void start(IJavaRemoConConfiguration configuration) throws Throwable {
	}
}
