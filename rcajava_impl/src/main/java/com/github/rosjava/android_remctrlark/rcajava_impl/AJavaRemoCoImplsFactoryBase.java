/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.rcajava_impl;

import com.github.rosjava.android_remctrlark.rcajava.IJavaRemoCoImpl;
import com.github.rosjava.android_remctrlark.rcajava.IJavaRemoCoImplsFactory;
import com.github.rosjava.android_remctrlark.rcajava.IJavaRemoCompImpl;
import com.github.rosjava.android_remctrlark.rcajava.IJavaRemoConImpl;
import com.google.common.base.CaseFormat;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * This is an implementation of a Java RemoCo factory. Implement the method {@link #registerAllRemoCoTypes()} for
 * registering the provided RemoCo types by this factory.
 */
public abstract class AJavaRemoCoImplsFactoryBase implements IJavaRemoCoImplsFactory {
	private final Map<String, Class<IJavaRemoConImpl>> mRemoConTypes = new HashMap<String, Class<IJavaRemoConImpl>>();
	private final Map<String, Class<IJavaRemoCompImpl>> mRemoCompTypes = new HashMap<String, Class<IJavaRemoCompImpl>>();

	protected AJavaRemoCoImplsFactoryBase() {
		registerAllRemoCoTypes();
	}

	@Override
	public final Set<String> getAllRemoConTypes() {
		return mRemoConTypes.keySet();
	}

	@Override
	public final Set<String> getAllRemoCompTypes() {
		return mRemoCompTypes.keySet();
	}

	@Override
	public final IJavaRemoConImpl createRemoConImpl(String type) {
		try {
			final Class<? extends IJavaRemoConImpl> remoConClass = mRemoConTypes.get(type);
			return remoConClass != null ? remoConClass.newInstance() : null;
		} catch (Throwable e) {
			return null;
		}
	}

	@Override
	public final IJavaRemoCompImpl createRemoCompImpl(String type) {
		try {
			final Class<? extends IJavaRemoCompImpl> remoCompClass = mRemoCompTypes.get(type);
			return remoCompClass != null ? remoCompClass.newInstance() : null;
		} catch (Throwable e) {
			return null;
		}
	}

	/**
	 * Call this method in your implementation of {@link #registerAllRemoCoTypes()} for registering a RemoCo type's
	 * implementing class. This method uses refection for recognizing whether the RemoCo type's implementing class
	 * implements a RemoCon or a RemoComp.
	 * The type identifier is derived implicitly from the class name by converting it from camel case to snake case.
	 * @param remoCoClass RemoCo type's implementing class.
	 */
	protected final void register(Class<? extends IJavaRemoCoImpl> remoCoClass) {
		final String type = CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, remoCoClass.getSimpleName());
		register(type, remoCoClass);
	}

	/**
	 * Call this method in your implementation of {@link #registerAllRemoCoTypes()} for registering a RemoCo type's
	 * implementing class. This method uses refection for recognizing whether the RemoCo type's implementing class
	 * implements a RemoCon or a RemoComp.
	 * @param type The RemoCo type identifier.
	 * @param remoCoClass RemoCo type's implementing class.
	 */
	@SuppressWarnings("unchecked")
	protected final void register(String type, Class<? extends IJavaRemoCoImpl> remoCoClass) {
		for (Class<?> currentClass = remoCoClass; currentClass != null; currentClass = currentClass.getSuperclass()) {
			for (Type currentInterface : currentClass.getGenericInterfaces()) {
				if (currentInterface.equals(IJavaRemoConImpl.class)) {
					mRemoConTypes.put(type, (Class<IJavaRemoConImpl>) remoCoClass);
					break;
				}
				if (currentInterface.equals(IJavaRemoCompImpl.class)) {
					mRemoCompTypes.put(type, (Class<IJavaRemoCompImpl>) remoCoClass);
					break;
				}
			}
		}
	}

	/**
	 * Implement the method by calling {@link #register(Class)} or {@link #register(String, Class)} for registering the
	 * provided RemoCo types by this factory.
	 */
	protected abstract void registerAllRemoCoTypes();
}
