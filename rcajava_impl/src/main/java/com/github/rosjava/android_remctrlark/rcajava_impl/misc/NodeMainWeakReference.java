/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.rcajava_impl.misc;

import org.ros.namespace.GraphName;
import org.ros.node.ConnectedNode;
import org.ros.node.Node;
import org.ros.node.NodeMain;

import java.lang.ref.WeakReference;

public final class NodeMainWeakReference<T extends NodeMain> implements NodeMain {

	private final int mHachCode;
	private GraphName mDefaultNodeName;
	private WeakReference<T> mRef;

	public NodeMainWeakReference(T nodeMain) {
		mHachCode = nodeMain.hashCode();
		mDefaultNodeName = nodeMain.getDefaultNodeName();
		mRef = new WeakReference<T>(nodeMain);
	}

	public T get() {
		if (mRef != null) {
			final T nodeMain = mRef.get();
			if (nodeMain != null) {
				return nodeMain;
			}
			mDefaultNodeName = null;
			mRef = null;
		}
		return null;
	}

	public WeakReference<T> ref() {
		return mRef;
	}

	@Override
	public GraphName getDefaultNodeName() {
		return mDefaultNodeName;
	}

	@Override
	public void onStart(ConnectedNode node) {
		final T nodeMain = get();
		if (nodeMain != null) {
			nodeMain.onStart(node);
		}
	}

	@Override
	public void onShutdown(Node node) {
		final T nodeMain = get();
		if (nodeMain != null) {
			nodeMain.onShutdown(node);
		}
	}

	@Override
	public void onShutdownComplete(Node node) {
		final T nodeMain = get();
		if (nodeMain != null) {
			nodeMain.onShutdownComplete(node);
		}
	}

	@Override
	public void onError(Node node, Throwable throwable) {
		final T nodeMain = get();
		if (nodeMain != null) {
			nodeMain.onError(node, throwable);
		}
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof NodeMainWeakReference) {
			final T nodeMain = get();
			return nodeMain != null && nodeMain.equals(((NodeMainWeakReference) o).get());
		} else {
			return super.equals(o);
		}
	}

	@Override
	public int hashCode() {
		return mHachCode;
	}
}
