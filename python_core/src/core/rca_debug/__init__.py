# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

_FALLBACK_RCA_CWD_REL_PATH = "../com.github.rosjava.android_remctrlark"
_CORE_RCA_REL_DIR = "core"
_SHARED_RCA_REL_DIR = "shared"
_EXTRAS_FOR_DESKTOP_RCA_REL_DIR = "core/extras_for_desktop"
_PREPARE_DEBUG_RCA_REL_FILE = "core/prepare-debug.sh"
_ENV_VAR_NAME_PORT = "RCA_AP_PORT"
_ENV_VAR_NAME_HOST = "RCA_AP_HOST"
_RCA_AP_PORT_DEFAULT = "9999"
_RCA_AP_HOST_DEFAULT = "localhost"


def set_logging_level_to_info():
    """
    @rtype: int
    """
    import logging

    return set_logging_level(logging.INFO)


def set_logging_level_to_debug():
    """
    @rtype: int
    """
    import logging

    return set_logging_level(logging.DEBUG)


def set_logging_level(level):
    """
    @rtype: int
    """
    from remctrlark.rca_logging.RcaGlobalLog import RcaGlobalLog

    RcaGlobalLog.set_level(level)
    return level


def load_test_env():
    import os
    import sys
    from subprocess import Popen, PIPE

    rca_ap_port = _RCA_AP_PORT_DEFAULT
    rca_ap_host = _RCA_AP_HOST_DEFAULT

    print "Loading RCA test environment for executing python:"
    print "  working dir: " + os.getcwd()
    print "  rca dir: " + _get_abs_rca_path(".")

    print "  calling script '%s':" % _PREPARE_DEBUG_RCA_REL_FILE
    p = Popen([_get_abs_rca_path(_PREPARE_DEBUG_RCA_REL_FILE)], stdout=PIPE)
    output_lines = p.stdout.read().splitlines()
    for line in output_lines:
        assert isinstance(line, str) or isinstance(line, unicode)
        print "    " + line
        if line.startswith(_ENV_VAR_NAME_PORT + "="):
            rca_ap_port = line[len(_ENV_VAR_NAME_PORT) + 1:]
        elif line.startswith(_ENV_VAR_NAME_HOST + "="):
            rca_ap_host = line[len(_ENV_VAR_NAME_HOST) + 1:]

    os.environ[_ENV_VAR_NAME_PORT] = rca_ap_port
    os.environ[_ENV_VAR_NAME_HOST] = rca_ap_host

    sys.path.append(_get_abs_rca_path(_SHARED_RCA_REL_DIR))
    sys.path.append(_get_abs_rca_path(_CORE_RCA_REL_DIR))
    sys.path.append(_get_abs_rca_path(_EXTRAS_FOR_DESKTOP_RCA_REL_DIR))

    print "  environment variables:"
    environment = os.environ.items()
    for item in environment:
        print "    %s=%s" % (item[0], str(item[1]))
    print


def _get_abs_rca_path(path):
    import os
    if os.path.exists(_CORE_RCA_REL_DIR) and os.path.isdir(_CORE_RCA_REL_DIR):
        rca_rel_path = "."
    else:
        rca_rel_path = _FALLBACK_RCA_CWD_REL_PATH
    return os.path.normpath(os.path.join(os.getcwd(), os.path.join(rca_rel_path, path)))
