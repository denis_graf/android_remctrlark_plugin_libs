#!/usr/bin/python2.7

# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

import getopt
import sys

from rca_debug import load_test_env, set_logging_level_to_info
from rca_debug import set_logging_level_to_debug


ARG_SCRIPT_ID = ["s", "script-id"]
ARG_LOG_LEVEL = ["l", "log-level"]
ARG_DETERMINE_ROS_IP = ["i", "determine-ros-ip"]


def help_doc():
    """
    @rtype: None
    """
    print "usage: [-%s <id> | -%s <INFO|DEBUG> | -%s]" % \
          ("|--".join(ARG_SCRIPT_ID),
           "|--".join(ARG_LOG_LEVEL),
           "|--".join(ARG_DETERMINE_ROS_IP))


def main(argv):
    """
    @type argv: list[str]
    @rtype: None
    """
    load_test_env()

    script_id = None
    log_level = "INFO"

    try:
        opts, args = getopt.getopt(argv, "h%s:%s:%s" % (ARG_SCRIPT_ID[0],
                                                        ARG_LOG_LEVEL[0],
                                                        ARG_DETERMINE_ROS_IP[0]),
                                   [ARG_SCRIPT_ID[1] + "=",
                                    ARG_LOG_LEVEL[1] + "=",
                                    ARG_DETERMINE_ROS_IP[1]])
    except getopt.GetoptError:
        help_doc()
        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            help_doc()
            sys.exit()
        elif opt in all_opts(ARG_SCRIPT_ID):
            script_id = arg
        elif opt in all_opts(ARG_LOG_LEVEL):
            log_level = arg
            # Note: ARG_DETERMINE_ROS_IP will be checked later.

    if log_level == "INFO":
        set_logging_level_to_info()
    elif log_level == "DEBUG":
        set_logging_level_to_debug()
    else:
        help_doc()
        sys.exit(2)

    if script_id is None:
        from remctrlark.rca_script.MainScript import MainScript

        script_id = MainScript.SCRIPT_ID

    from remctrlark.application.RcaScriptStarter import start_script

    # Start script in current process.
    start_script(script_id)


def all_opts(arg_names):
    """
    @tpye arg_names: list[str]
    @rtype: tuple[str]
    """
    return "-" + arg_names[0], "--" + arg_names[1]


if __name__ == "__main__":
    from remctrlark.error_handling.RcaException import RcaException

    try:
        main(sys.argv[1:])
    except RcaException as e:
        if str(e).startswith("java.lang.SecurityException"):
            from remctrlark.rca_logging.RcaGlobalLog import RcaGlobalLog
            import os

            RcaGlobalLog(os.path.basename(sys.argv[0])).e(
                "Authentication failed! "
                "For debugging python, please deactivate 'handshake' in RCA main application settings.")
        raise
