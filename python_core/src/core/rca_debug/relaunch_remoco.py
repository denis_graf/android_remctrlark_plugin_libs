#!/usr/bin/python2.7

# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

import getopt
import sys

from rca_debug import load_test_env, set_logging_level_to_info
from rca_debug import set_logging_level_to_debug


ARG_REMOCO_ID = ["r", "remoco-id"]
ARG_LOG_LEVEL = ["l", "log-level"]


def help_doc():
    """
    @rtype: None
    """
    print "usage: [-%s <id> | -%s <INFO|DEBUG>]" % \
          ("|--".join(ARG_REMOCO_ID),
           "|--".join(ARG_LOG_LEVEL))


def main(argv):
    """
    @type argv: list[str]
    @rtype: None
    """
    load_test_env()

    remoco_id = None
    log_level = "INFO"

    try:
        opts, args = getopt.getopt(argv, "h%s:%s:" % (ARG_REMOCO_ID[0],
                                                      ARG_LOG_LEVEL[0]),
                                   [ARG_REMOCO_ID[1] + "=",
                                    ARG_LOG_LEVEL[1] + "="])
    except getopt.GetoptError:
        help_doc()
        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            help_doc()
            sys.exit()
        elif opt in all_opts(ARG_REMOCO_ID):
            remoco_id = arg
        elif opt in all_opts(ARG_LOG_LEVEL):
            log_level = arg

    if log_level == "INFO":
        set_logging_level_to_info()
    elif log_level == "DEBUG":
        set_logging_level_to_debug()
    else:
        help_doc()
        sys.exit(2)

    if remoco_id is None:
        help_doc()
        sys.exit(2)

    from remctrlark.rca_proxy_wrappers.RcaIPyQueryProxy import RcaIPyQueryProxy

    with RcaIPyQueryProxy() as proxy:
        proxy.rca_relaunch_remoco(remoco_id)


def all_opts(arg_names):
    """
    @tpye arg_names: list[str]
    @rtype: tuple[str]
    """
    return "-" + arg_names[0], "--" + arg_names[1]


if __name__ == "__main__":
    main(sys.argv[1:])
