#!/bin/bash

# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

# Project
PROJECT_PACKAGE=com.github.rosjava.android_remctrlark
PYTHON_PROJECT_ZIP_FILE_REL_TO_MAIN_PROJECT=rca_plugin_python/build/rca_plugin_python.zip

# Debugging
PYTHON_AP_PORT_REMOTE=45007
PYTHON_AP_PORT_LOCAL=9999

# External
RUN_ADB=adb
RUN_PYTHON27=python2.7
RUN_IPYTHON27=ipython2.7
RUN_ZIP=zip
RUN_UNZIP=unzip

# Internal
EXTERNAL_STORAGE_ON_DEVICE_DIR=$($RUN_ADB shell echo -n '$EXTERNAL_STORAGE')
PACKAGE_ON_DEVICE_PATH=$EXTERNAL_STORAGE_ON_DEVICE_DIR/$PROJECT_PACKAGE/
IGNORE_DIR_IN_ZIP=core

# Load project local config.
pushd `dirname $0` > /dev/null
SCRIPT_DIR=`pwd`  # Note: Don't use paramater -P here, because we don't want to resolve symlinks!
popd > /dev/null

# Note: Don't use .. operator here, because we don't want to resolve symlinks!
PYTHON_CORE_SRC_DIR=$(dirname $SCRIPT_DIR)
PYTHON_PROJECT_DIR=$(dirname $PYTHON_CORE_SRC_DIR)
PYTHON_PROJECT_SRC_DIR=$PYTHON_PROJECT_DIR/src
MAIN_PROJECT_DIR=$(dirname $PYTHON_PROJECT_DIR)
PYTHON_PROJECT_CONFIG=$PYTHON_PROJECT_DIR/scripts-config.sh
MAIN_PROJECT_CONFIG=$MAIN_PROJECT_DIR/scripts-config.sh

# Look for project local config.
if [ -e $PYTHON_PROJECT_CONFIG ]; then
  . $PYTHON_PROJECT_CONFIG
else
	if [ -e $MAIN_PROJECT_CONFIG ]; then
	  . $MAIN_PROJECT_CONFIG
	  PYTHON_PROJECT_CONFIG=$MAIN_PROJECT_CONFIG
	else
	  echo "Error: No local config for project found! Please create $MAIN_PROJECT_CONFIG"
	fi
fi

# Internal
PYTHON_PROJECT_ZIP_FILE_REL_TO_PYTHON_PROJECT=../$PYTHON_PROJECT_ZIP_FILE_REL_TO_MAIN_PROJECT
PYTHON_PROJECT_ZIP_FILE_ABS=$MAIN_PROJECT_DIR/$PYTHON_PROJECT_ZIP_FILE_REL_TO_MAIN_PROJECT
TEMP_UNZIP_DIR=$PYTHON_PROJECT_DIR/temp_unzipped_${PROJECT_PACKAGE}
START_SCRIPT_FILE=${SCRIPT_DIR}/rca_debug/start_script.py
RELAUNCH_REMOCO=${SCRIPT_DIR}/rca_debug/relaunch_remoco.py
RUN_PREPARE_DEBUG=${SCRIPT_DIR}/prepare-debug.sh
RUN_UPDATE_SCRIPT=${SCRIPT_DIR}/zip-project.sh
RCA_PYTHONPATH=$PYTHONPATH:$PYTHON_CORE_SRC_DIR/core
