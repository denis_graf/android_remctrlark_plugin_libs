#!/bin/bash

# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

SCRIPT_DIR=$(dirname $0)
. ${SCRIPT_DIR}/scripts-config.sh
if [ ! -e $PYTHON_PROJECT_CONFIG ]; then
  exit 1
fi

if ! $RUN_PREPARE_DEBUG ; then
  exit 1
fi

cd $PYTHON_PROJECT_SRC_DIR
PYTHONPATH=${RCA_PYTHONPATH} $RUN_PYTHON27 $START_SCRIPT_FILE $@
