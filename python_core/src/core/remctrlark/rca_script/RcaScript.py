# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from Queue import Queue
from abc import ABCMeta, abstractmethod
import threading

from remctrlark.binders.ARcaBinder import ARcaBinder
from remctrlark.binders.ReflectingBinder import ReflectingBinder
from remctrlark.error_handling.RcaException import RcaException
from remctrlark.error_handling.handle_exception import handle_exception
from remctrlark.event_handling.Sl4aEventsListener import Sl4aEventsListener
from remctrlark.rca_proxy_wrappers.ProxyProxy import ProxyProxy
from remctrlark.rca_proxy_wrappers.ScriptBinderServerProxy import ScriptBinderServerProxy
from .ARcaScript import ARcaScript
from remctrlark.rca_proxy_wrappers.ScriptProcessMessengerProxy import ScriptProcessMessengerProxy


_EVENT_RCA_CALL_METHOD = 'rca.rpc.event.call'

_INTERNAL_JAVA_METHODS_PREFIX = "rca.script.java."
_INTERNAL_PYTHON_METHODS_PREFIX = "rca.script.python."


class RcaScript(ARcaScript):
    _MSG_SCRIPT_EXIT_REACHED = "rca.script_exit_reached"

    class _ATask(object):
        __metaclass__ = ABCMeta

        @abstractmethod
        def do(self):
            pass

        def __str__(self):
            return self.__class__.__name__

    class _TaskRun(_ATask):
        def __init__(self, runnable):
            """
            @type runnable: ( () -> None ) | ( (object) -> None )
            """
            super(RcaScript._TaskRun, self).__init__()
            self._runnable = runnable

        def do(self):
            self._runnable()

        def __str__(self):
            return super(RcaScript._TaskRun, self).__str__() + ": " + str(self._runnable)

    class _TaskEvent(_ATask):
        def __init__(self, handler, name, data):
            """
            @type handler: (RcaScript, str, dict[str, object]) -> None
            @type name: str
            @type data: dict[str, object]
            """
            super(RcaScript._TaskEvent, self).__init__()
            self._handler = handler
            self._name = name
            self._data = data

        def do(self):
            self._handler(self._name, self._data)

        def __str__(self):
            return super(RcaScript._TaskEvent, self).__str__() +\
                (": {name: %s, data: %s}" % (self._name, str(self._data)))

    class _BinderServer(object):
        def __init__(self, parent, binder, exception_handler):
            """
            @type parent: RcaScript
            @type binder: ARcaBinder | None
            @type exception_handler: ( () -> object ) or ( (object) -> object ) | None
            """
            super(RcaScript._BinderServer, self).__init__()
            self._parent = parent
            self._binder = binder

            methods_info = list()
            if self._binder is not None:
                methods = self._binder.get_methods()
                for method in methods:
                    methods_info.append({"name": method.get_name(), "doc": method.get_doc()})

            self._proxy_out = ScriptBinderServerProxy(self._parent.script_id, methods_info)
            self._proxy_in = ProxyProxy(self._proxy_out)
            self._proxy_in.rca_post_initialized_event()
            self._events_listener = \
                Sl4aEventsListener(self._proxy_in, self._handle_event, exception_handler)

        def start(self):
            self._events_listener.start()

        def shutdown(self):
            if self._events_listener is not None:
                self._events_listener.shutdown()
                self._events_listener = None
                """@type: Sl4aEventsListener"""
            if self._proxy_in is not None:
                self._proxy_in.shutdown()
                self._proxy_in = None
                """@type: ProxyProxy"""
            if self._proxy_out is not None:
                self._proxy_out.shutdown()
                self._proxy_out = None
                """@type: ScriptBinderServerProxy"""
            self._binder = None
            """@type: ARcaBinder"""

        def _handle_event(self, name, data):
            """
            @type name: str
            @type data: dict[str, object]
            """
            if name == _EVENT_RCA_CALL_METHOD:
                call_id = data['rpc_id']
                method = data['name']
                args = data['args']

                def run():
                    result = None
                    success = False
                    # noinspection PyBroadException
                    try:
                        if self._binder is None:
                            raise RcaException("No binder set for %s." % RcaScript._BinderServer.__name__)

                        # TODO: throw explicit exception if no method found?
                        result = self._binder.invoke_method(method, args)
                        success = True
                    except:
                        self._proxy_out.rca_handle_error(call_id)

                    if success:
                        self._proxy_out.rca_handle_result(call_id, result)

                # noinspection PyBroadException
                try:
                    posted = True
                    self._parent.sync_run_on_script_thread(run)
                    # posted = self._parent.post_to_script_thread(run)
                    if not posted:
                        raise RcaException("Could not post action method invocation.")
                except:
                    self._proxy_out.rca_handle_error(call_id)
            elif name == ProxyProxy.EVENT_INITIALIZED:
                self._parent._post_initialized_notification()
            else:
                self._proxy_out.log.w("ignoring unknown event: {name: '%s', data: %s}" % (name, str(data)))

    def __init__(self, proxy, script_id):
        """
        @type proxy: AScriptProxy
        @type script_id: str
        """
        self.__main_thread_ident = threading.current_thread().ident

        super(RcaScript, self).__init__()
        self.script_id = script_id

        if __debug__:
            self.__started = False

        self.__proxy = proxy
        self._rca = self.__proxy.rca
        self._log = self.__proxy.log

        self.__binder_server = None
        """@type: RcaScript._BinderServer"""
        self.__queue = Queue()
        self.__shutdown_requested = False

    def start(self):
        self._run()
        # noinspection PyBroadException
        try:
            self._on_script_exit_reached()
        except:
            pass

    def shutdown(self):
        if not self.__shutdown_requested:
            self.__shutdown_requested = True
            # Wake up the queue:
            self.__queue.put(None)

    def _run(self):
        if __debug__:
            assert not self.__started, "The RCA-script can be started only once!"
            self.__started = True
        # noinspection PyBroadException
        try:
            self._on_initialized()
            self.__main_loop()
        except:
            handle_exception()

        # noinspection PyBroadException
        try:
            self.__finish()
        except:
            handle_exception()

    @staticmethod
    def _on_script_exit_reached():
        with ScriptProcessMessengerProxy() as proxy:
            proxy.rca_send_message(RcaScript._MSG_SCRIPT_EXIT_REACHED)

    def __main_loop(self):
        while not (self.__shutdown_requested and self.__queue.empty()):
            if __debug__:
                self._log.d("%s() waiting for tasks..." % self.__main_loop.__name__)
            task = None
            if __debug__:
                # Note: Python Issue 1360: Queue.get() can't be interrupted with Ctrl-C unless timed out
                # As a workaround for this issue we set the timer to a very high value and ignore the timeout.
                from Queue import Empty
                while True:
                    try:
                        task = self.__queue.get(timeout=3628800)
                        break
                    except Empty:
                        continue
            else:
                task = self.__queue.get()

            if __debug__:
                self._log.d("%s() got a task: %s" % (self.__main_loop.__name__, str(task)))
            if task is not None:
                assert isinstance(task, self._ATask)
                task.do()
            self.__queue.task_done()

    def post_to_script_thread(self, runnable):
        """
        @type runnable: ( () -> None ) | ( (object) -> None )
        @rtype: bool
        """
        if self.__shutdown_requested:
            return False
        self.__queue.put(self._TaskRun(runnable))
        return True

    def run_on_script_thread(self, func, *args):
        """
        @type func: callable
        @type args: tuple
        @rtype: bool
        """
        if self._is_on_main_thread():
            func(*args)
            return True

        def run():
            func(*args)

        return self.post_to_script_thread(run)

    def sync_run_on_script_thread(self, func, *args):
        """
        @type func: callable
        @type args: tuple
        @rtype: object | None
        """
        if self._is_on_main_thread():
            return func(*args)
        else:
            result = [None]
            exception = [None]
            done = threading.Event()

            def run():
                try:
                    result[0] = func(*args)
                except Exception as e:
                    exception[0] = e
                    raise
                finally:
                    done.set()

            if self.post_to_script_thread(run):
                done.wait()
            else:
                exception[0] = RcaException("Call could not be posted to script's thread.")

            if exception[0] is not None:
                raise RcaException("Error on executing a call on script's thread.", exception[0])

            return result[0]

    def __finish(self):
        self._on_finished()
        self.__proxy.shutdown()
        self._rca = None
        self._log = None

    def _on_initialized(self):
        self._log.i(RcaScript._on_initialized.__name__)

        binder = self._create_binder()

        # TODO: Issue #19: Catching of Uncaught Exceptions in Child-Thdreads/-Processes of
        # Python-RemoComps-Implementations -> take this implementation exemplary for other threads...
        def exception_handler():
            exc_value = handle_exception(False)

            def raise_exception():
                raise RcaException("Error on script's binder server thread.", exc_value)

            self.post_to_script_thread(raise_exception)

        self.__binder_server = \
            self._BinderServer(self, binder, exception_handler)
        self.__binder_server.start()

    def _on_finished(self):
        self._log.i(RcaScript._on_finished.__name__)

        if self.__binder_server is not None:
            self.__binder_server.shutdown()
            self.__binder_server = None
            """@type: RcaScript._BinderServer"""

    def _create_binder(self):
        """
        @rtype: ARcaBinder
        """
        return _ScriptTwoWaysBinder(self._create_internal_script_binder(), self._create_public_script_binder())

    def _create_public_script_binder(self):
        """
        @rtype: rcapy.AReflectingBinderImpl
        """
        return None

    def _create_internal_script_binder(self):
        """
        @rtype: rcapy.AReflectingBinderImpl | list | tuple | None
        """
        return None

    def _is_on_main_thread(self):
        """
        @rtype: bool
        """
        return self.__main_thread_ident == threading.current_thread().ident

    def _post_initialized_notification(self):
        self.post_to_script_thread(self.__rca_on_initialized)

    def __rca_on_initialized(self):
        """
        @rtype: None
        """
        self._rpc_invoke_internal_method("onScriptInitialized", ())

    def _rpc_invoke_internal_method(self, name, args):
        """
        @type name: str
        @type args: tuple
        @rtype: object
        """
        return self._rca.invoke_method(_INTERNAL_JAVA_METHODS_PREFIX + name, args)


class _ScriptTwoWaysBinder(ARcaBinder):
    __INTERNAL_METHODS_PREFIX_LEN = len(_INTERNAL_PYTHON_METHODS_PREFIX)

    def __init__(self, internal_binder_impl, public_binder_impl):
        """
        @type internal_binder_impl: rcapy.AReflectingBinderImpl | list | tuple | None
        @type public_binder_impl: rcapy.AReflectingBinderImpl | list | tuple | None
        """
        super(_ScriptTwoWaysBinder, self).__init__()
        if internal_binder_impl is None:
            self._internal_binder = None
        else:
            self._internal_binder = ReflectingBinder(internal_binder_impl)
        if public_binder_impl is None:
            self._public_binder = None
        else:
            self._public_binder = ReflectingBinder(public_binder_impl)

    def invoke_method(self, name, args):
        """
        @type name: str
        @type args: tuple
        @rtype: object
        """
        if name.startswith(_INTERNAL_PYTHON_METHODS_PREFIX):
            return self._internal_binder.invoke_method(name[_ScriptTwoWaysBinder.__INTERNAL_METHODS_PREFIX_LEN:], args)
        if self._public_binder is not None:
            return self._public_binder.invoke_method(name, args)

    def get_methods(self):
        """
        @rtype: tuple[ARcaBinderMethod] | list[ARcaBinderMethod]
        """
        if self._public_binder is not None:
            return self._public_binder.get_methods()
        return list()
