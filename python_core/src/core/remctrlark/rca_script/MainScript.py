# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

import multiprocessing
from threading import Thread

from .RcaScript import RcaScript
from remctrlark.application.RcaScriptStarter import start_script_as_subprocess
from remctrlark.error_handling.RemoCoScriptCouldNotBeStartedException import RemoCoScriptCouldNotBeStartedException
from remctrlark.error_handling.handle_exception import handle_exception
from remctrlark.rca_proxy_wrappers.MainScriptProxy import MainScriptProxy


class MainScript(RcaScript):
    SCRIPT_ID = "rca.mainscript"

    def __init__(self):
        proxy = MainScriptProxy(MainScript.SCRIPT_ID)
        super(MainScript, self).__init__(proxy, proxy.get_script_id())
        self._script_threads = dict()
        """@type: dict[str, Thread]"""

    def _create_public_script_binder(self):
        """
        @rtype: rcapy.AReflectingBinderImpl
        """
        def remoco_script_start(script_id):
            """
            @type script_id: str
            """
            def run():
                # noinspection PyBroadException
                try:
                    self._remoco_script_start(script_id)
                except:
                    handle_exception()

            # Run on an own thread for allowing starting RemoComps in parallel.
            script_thread = self._script_threads.get(script_id)
            if script_thread is not None:
                assert isinstance(script_thread, Thread)
                self.__join_script_thread(script_id, script_thread)

            script_thread = Thread(target=run)
            self._script_threads[script_id] = script_thread
            script_thread.start()

        def main_script_shutdown():
            self.shutdown()
            for script_thread_item in self._script_threads.items():
                script_id = script_thread_item[0]
                script_thread = script_thread_item[1]
                assert isinstance(script_id, str) | isinstance(script_id, unicode)
                assert isinstance(script_thread, Thread)
                self.__join_script_thread(script_id, script_thread)
            self._script_threads = None
            self._log.i("all script threads stopped")

        return remoco_script_start, main_script_shutdown

    def _remoco_script_start(self, script_id):
        """
        @type script_id: str
        """
        if script_id.startswith(MainScript.SCRIPT_ID):
            self._log.w("invalid request for starting remo-co script '%s'" % script_id)
        else:
            self._log.i("request for starting remo-co script '%s'" % script_id)

            done = multiprocessing.Manager().Event()
            exception = multiprocessing.Manager().Value(dict, "null", lock=False)

            def on_started():
                done.set()

            def on_exception():
                exception.value = handle_exception(False)
                done.set()

            start_script_as_subprocess(script_id, on_started=on_started, exception_handler=on_exception)
            done.wait()

            if exception.value != "null":
                raise RemoCoScriptCouldNotBeStartedException(script_id, exception.value)

    def __join_script_thread(self, script_id, script_thread):
        """
        @type script_id: str | unicode
        @type script_thread: Thread
        @rtype: None
        """
        self._log.i("waiting for thread to stop of script '%s'..." % script_id)
        script_thread.join()
        self._log.i("thread for script '%s' stopped" % script_id)
