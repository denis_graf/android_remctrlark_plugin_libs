# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from remctrlark.rca_proxy_wrappers.ARcaProxyWrapperWithLogger import ARcaProxyWrapperWithLogger


class ScriptBinderServerProxy(ARcaProxyWrapperWithLogger):
    def __init__(self, name, methods_info):
        """
        @type name: str
        @type methods_info: list[dict[str, object]]
        """
        self._name = name
        self._methods_info = methods_info
        super(ScriptBinderServerProxy, self).__init__("%s:%s" % (self.__class__.__name__, self._name))

    def _bind_to_rca(self):
        self._proxy.rca_bind_as_script_binder_server(self._name, self._methods_info)

    def rca_handle_result(self, call_id, result):
        """
        @type call_id: int
        @type result: object
        """
        self._rca.invoke_method("rca.rpc.handleResult", (call_id, result))

    def rca_handle_error(self, call_id):
        """
        @type call_id: int
        """
        from remctrlark.error_handling.handle_exception import get_exc_info

        self._rca.invoke_method("rca.rpc.handleError", (call_id, get_exc_info()))
