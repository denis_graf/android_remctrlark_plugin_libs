# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

import time
from .ARcaProxyWrapper import ARcaProxyWrapper
from remctrlark.rca_logging.ARcaLog import ARcaLog
from remctrlark.rca_logging.RcaGlobalLog import RcaGlobalLog

_DEBUG = False


class LoggerProxy(ARcaProxyWrapper):
    def __init__(self, is_remoco_impl_log, tag):
        """
        @type is_remoco_impl_log: bool
        @type tag: str
        """
        self.__is_remoco_impl_log = is_remoco_impl_log
        self.__tag = tag
        if __debug__ and _DEBUG:
            log = RcaGlobalLog(self.__class__.__name__)
        else:
            log = _DummyLogger()
        super(LoggerProxy, self).__init__(log)

    def __enter__(self):
        """
        @rtype: LoggerProxy
        """
        return super(LoggerProxy, self).__enter__()

    def _bind_to_rca(self):
        self._proxy.rca_bind_as_logger(self.__is_remoco_impl_log, self.__tag)

    def rca_log(self, log_level, message):
        """
        @type log_level: str
        @type message: str
        @rtype: None
        """
        self._rca.log(int(round(time.time() * 1000)), log_level, message)


class _DummyLogger(ARcaLog):
    def __init__(self):
        super(_DummyLogger, self).__init__(None)

    def e_exc(self, msg):
        pass

    def i(self, msg):
        pass

    def w(self, msg):
        pass

    def e(self, msg):
        pass

    def d(self, msg):
        pass
