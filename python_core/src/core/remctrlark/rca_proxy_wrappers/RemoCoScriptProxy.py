# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from .AScriptProxy import AScriptProxy


class RemoCoScriptProxy(AScriptProxy):
    def __init__(self, script_id):
        """
        @type script_id: str
        """
        self._script_info = None
        super(RemoCoScriptProxy, self).__init__(script_id)

    def _bind_to_rca(self):
        self._script_info = self._proxy.rca_bind_as_remoco_script(self._script_id)

    def get_script_info(self):
        """
        @rtype: dict[str, object]
        """
        return self._script_info
