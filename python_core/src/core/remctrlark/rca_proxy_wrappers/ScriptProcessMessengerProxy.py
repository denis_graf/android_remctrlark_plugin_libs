# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from remctrlark.rca_proxy_wrappers.ARcaProxyWrapperWithLogger import ARcaProxyWrapperWithLogger


class ScriptProcessMessengerProxy(ARcaProxyWrapperWithLogger):
    def __init__(self):
        super(ScriptProcessMessengerProxy, self).__init__(self.__class__.__name__)

    def __enter__(self):
        """
        @rtype: ScriptProcessMessengerProxy
        """
        return super(ScriptProcessMessengerProxy, self).__enter__()

    def _bind_to_rca(self):
        self._proxy.rca_bind_as_script_process_messenger()

    def rca_send_message(self, message):
        self._rca.sendMessage(message)
