# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from abc import abstractmethod, ABCMeta

from remctrlark.binders.RcaProxyBinder import RcaProxyBinder
from remctrlark.event_handling.RcaEvent import RcaEvent
from remctrlark.rca_proxy.ARcaProxy import ARcaProxy


class ARcaProxyWrapper(object):
    __metaclass__ = ABCMeta

    class RcaProxy(ARcaProxy):
        def __init__(self, parent, addr=None):
            """
            @type parent: ARcaProxyWrapper
            @param addr: tuple | None
            """
            super(ARcaProxyWrapper.RcaProxy, self).__init__(parent.__class__.__name__, addr)
            self._parent = parent

        def sl4a_request_access_to_blocked_proxy(self):
            self._parent._on_request_access_on_blocked_proxy()

        def sl4a_debug_on_blocked_proxy(self, method, args):
            """
            @type method: str
            @type args: tuple
            """
            self._parent._sl4a_debug_on_blocked_proxy(method, args)

    def __init__(self, log):
        """
        @type log: ARcaLog
        """
        super(ARcaProxyWrapper, self).__init__()

        self.log = log
        self._proxy = ARcaProxyWrapper.RcaProxy(self)
        self._rca = RcaProxyBinder(self._proxy, self.log)

        self._bind_to_rca()

        if __debug__:
            self.log.d("proxy started")

    def __enter__(self):
        """
        @rtype: ARcaProxyWrapper
        """
        return self

    # noinspection PyUnusedLocal
    def __exit__(self, t, v, tb):
        self.shutdown()

    @property
    def rca_rid(self):
        """
        @rtype: int
        """
        return self._proxy.rca_rid

    def shutdown(self):
        if self._proxy is not None:
            if __debug__:
                self.log.d("proxy shut down")
            self._proxy.sl4a_shutdown()
            self._proxy = None
            """@type: ARcaProxyWrapper.RcaProxy"""
        self.log = None
        """@type: ARcaLog"""
        self._rca = None
        """@type: RcaProxyBinder"""

    def _sl4a_debug_on_blocked_proxy(self, method, args):
        """
        @type method: str
        @type args: tuple
        """
        self.log.w("wait for access to a blocked proxy on RPC: %s%s" % (method, args))

    def _on_request_access_on_blocked_proxy(self):
        """
        Note: Calling self._proxy.rca_interrupt_wait_for_event() does only make sense when the proxy is blocked by some
        routine which is waiting for an event. But that described case is handled explicitly in Sl4aEventsListener.
        Therefore, calling such routines on other places (not by Sl4aEventsListener) must be avoided! A general
        mechanism could be implemented, but it would be overhead to use it in all cases!
        """
        self.log.w("ignoring request for access to a blocked proxy")

    @abstractmethod
    def _bind_to_rca(self):
        pass

    def event_wait_for(self):
        """
        @rtype: RcaEvent | None
        """
        if __debug__:
            self.log.d("%s() listening..." % self.event_wait_for.__name__)
        event = self._proxy.rca_wait_for_event()
        if __debug__:
            self.log.d("%s() received event: %s" % (self.event_wait_for.__name__, str(event)))
        return event

    def event_interrupt_wait_for(self):
        if __debug__:
            self.log.d("%s()" % self.event_interrupt_wait_for.__name__)
        self._proxy.rca_interrupt_wait_for_event()
