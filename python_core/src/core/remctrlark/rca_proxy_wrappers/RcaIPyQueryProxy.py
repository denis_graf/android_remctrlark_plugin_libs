# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from remctrlark.binders.Sl4aProxyBinder import Sl4aProxyBinder
from remctrlark.rca_proxy_wrappers.ARcaProxyWrapperWithLogger import ARcaProxyWrapperWithLogger


class RcaIPyQueryProxy(ARcaProxyWrapperWithLogger):
    def __init__(self):
        super(RcaIPyQueryProxy, self).__init__(self.__class__.__name__)
        self._sl4a = Sl4aProxyBinder(self._proxy, self.log)

    def __enter__(self):
        """
        @rtype: RcaIPyQueryProxy
        """
        return super(RcaIPyQueryProxy, self).__enter__()

    @property
    def sl4a(self):
        return self._sl4a

    def _bind_to_rca(self):
        self._proxy.rca_bind_as_ipyquery()

    def rca_get_scripts(self):
        return self._rca.getScripts()

    def rca_script_relaunch(self, script_id):
        """
        @type script_id: str
        """
        self._rca.scriptRelaunch(script_id)

    def rca_get_script_info(self, script_id):
        """
        @type script_id: str
        """
        return self._rca.getScriptInfo(script_id)

    def rca_get_proxies(self):
        return self._rca.getProxies()

    def rca_get_remocon(self):
        return self._rca.getRemoCon()

    def rca_get_remocomps(self):
        return self._rca.getRemoComps()

    def rca_get_remoco_info(self, remoco_id):
        """
        @type remoco_id: str
        """
        return self._rca.getRemoCoInfo(remoco_id)

    def rca_get_remoco_detailed_info(self, remoco_id):
        """
        @type remoco_id: str
        """
        return self._rca.getRemoCoDetailedInfo(remoco_id)

    def rca_get_remoco_binder_methods(self, remoco_id):
        """
        @type remoco_id: str
        """
        return self._rca.getRemoCoBinderMethods(remoco_id)

    def rca_get_remoco_binder_method(self, remoco_id, method_name):
        """
        @type remoco_id: str
        @type method_name: str
        """
        return self._rca.getRemoCoBinderMethod(remoco_id, method_name)

    def rca_dispatch_event(self, dispatcher_id, event_name, data):
        """
        @type dispatcher_id: str
        @type event_name: str
        @type data: dict[str, object]
        """
        self._rca.dispatchEvent(dispatcher_id, event_name, data)

    def rca_call_remoco_binder_method(self, remoco_id, method_name, args):
        """
        @type remoco_id: str
        @type method_name: str
        @type args: tuple
        """
        return self._rca.callRemoCoBinderMethod(remoco_id, method_name, args)

    def rca_relaunch_remoco(self, remoco_id):
        """
        @type remoco_id: str
        """
        self._rca.relaunchRemoCo(remoco_id)

    def rca_remocon_relauncher_commit(self, commit):
        """
        @type commit: dict[str, object]
        """
        self._rca.RemoConRelauncher_commit(commit)
