# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from remctrlark.rca_proxy_wrappers.ARcaProxyWrapperWithLogger import ARcaProxyWrapperWithLogger


class RemoCoProxy(ARcaProxyWrapperWithLogger):
    def __init__(self, remoco_id):
        """
        @type remoco_id: str
        """
        self._remoco_id = remoco_id
        self._remoco_info = None
        super(RemoCoProxy, self).__init__(self.__class__.__name__ + ":" + remoco_id)

    def _bind_to_rca(self):
        self._remoco_info = self._proxy.rca_bind_as_remoco(self._remoco_id)

    @property
    def rca(self):
        """
        @rtype: RcaProxyBinder
        """
        return self._rca

    def get_remoco_info(self):
        """
        @rtyoe: dict[str, object]
        """
        return self._remoco_info

    def get_remoco_id(self):
        """
        @rtype: str
        """
        return self._remoco_id
