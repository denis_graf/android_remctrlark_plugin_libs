# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

import os
import rospy

from remctrlark.rca_logging.RcaLogWrapper import RcaLogWrapper
from remctrlark.rca_logging.RcaGlobalLog import RcaGlobalLog


class RcaProxyWrapperLog(RcaLogWrapper):
    def __init__(self, tag, is_remoco_impl_log=False):
        """
        @type tag: str
        @type is_remoco_impl_log: bool
        """
        super(RcaProxyWrapperLog, self).__init__(RcaGlobalLog(None), tag)

        from remctrlark.rca_proxy_wrappers.LoggerProxy import LoggerProxy

        if os.getenv("RCA_LOGCAT_DISABLED") is None:
            self._rca_log = LoggerProxy(is_remoco_impl_log, self._append_tag(""))
        else:
            self._rca_log = None
            """@type: LoggerProxy """

    def shutdown(self):
        if self._rca_log is not None:
            self._rca_log.shutdown()

    def e(self, msg):
        """
        @type msg: str
        """
        super(RcaProxyWrapperLog, self).e(msg)
        self._try_external_log(msg, rospy.logerr)
        if self._rca_log is not None:
            self._try_rca_log(msg, "ERROR")

    def e_exc(self, msg):
        """
        @type msg: str
        """
        super(RcaProxyWrapperLog, self).e_exc(msg)
        self._try_external_log(msg, rospy.logerr)
        if self._rca_log is not None:
            self._try_rca_log(msg, "ERROR")

    def d(self, msg):
        """
        @type msg: str
        """
        if __debug__:
            super(RcaProxyWrapperLog, self).d(msg)
            self._try_external_log(msg, rospy.logdebug)
            if self._rca_log is not None:
                self._try_rca_log(msg, "DEBUG")

    def i(self, msg):
        """
        @type msg: str
        """
        super(RcaProxyWrapperLog, self).i(msg)
        self._try_external_log(msg, rospy.loginfo)
        if self._rca_log is not None:
            self._try_rca_log(msg, "INFO")

    def w(self, msg):
        """
        @type msg: str
        """
        super(RcaProxyWrapperLog, self).w(msg)
        self._try_external_log(msg, rospy.logwarn)
        if self._rca_log is not None:
            self._try_rca_log(msg, "WARN")

    def _try_external_log(self, msg, log_func, tag_prefix=""):
        """
        @type msg: str
        @type log_func: ( (str) -> None ) | ( (object, str) -> None )
        @type tag_prefix: str
        """
        # noinspection PyBroadException
        try:
            log_func(tag_prefix + self._append_tag(msg))
        except:
            # Ignore all errors when logging...
            pass

    def _try_rca_log(self, msg, log_level):
        """
        @type msg: str
        @type log_level: str
        """
        # noinspection PyBroadException
        try:
            self._rca_log.rca_log(log_level, msg)
        except:
            # Ignore all errors when logging...
            pass
