# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

import rcapy


class RemoConRelauncher(rcapy.ARemoConRelauncher):
    def __init__(self, commit_func):
        """
        @type commit_func: ( (dict[str, object]) -> None ) | ( (object, dict[str, object]) -> None )
        """
        super(RemoConRelauncher, self).__init__()
        self._commit_func = commit_func
        self._commit = dict()
        self._clear()

    def connect_to_master(self, uri):
        """
        @type uri: str | None
        @rtype: ARemoConRelauncher
        """
        self._commit["connect_to_master"] = uri
        return self

    def connect_to_new_public_master(self):
        """
        @rtype: ARemoConRelauncher
        """
        self._commit["connect_to_new_public_master"] = True
        return self

    def connect_to_new_private_master(self):
        """
        @rtype: ARemoConRelauncher
        """
        self._commit["connect_to_new_private_master"] = True
        return self

    def set_namespace(self, nsid, namespace):
        """
        @type nsid: str
        @type namespace: str
        @rtype: ARemoConRelauncher
        """
        set_namespace = dict()
        set_namespace["nsid"] = nsid
        set_namespace["namespace"] = namespace
        self._commit["set_namespace"].append(set_namespace)
        return self

    def set_type(self, tid, remoco_type, apk=None):
        """
        @type tid: str
        @type remoco_type: str
        @type apk: str | None
        @rtype: ARemoConRelauncher
        """
        set_type = dict()
        set_type["tid"] = tid
        set_type["type"] = remoco_type
        if apk is not None:
            set_type["apk"] = apk
        self._commit["set_type"].append(set_type)
        return self

    def set_param(self, name, value):
        """
        @type name: str
        @type value: str | int | float | bool
        @rtype: ARemoConRelauncher
        """
        set_param = dict()
        set_param["name"] = name
        set_param["value"] = value
        self._commit["set_param"].append(set_param)
        return self

    def set_remocomp_param(self, remocomp_id, name, value):
        """
        @type remocomp_id: str
        @type name: str
        @type value: str | int | float | bool
        @rtype: ARemoConRelauncher
        """
        set_remocomp_param = dict()
        set_remocomp_param["id"] = remocomp_id
        set_remocomp_param["name"] = name
        set_remocomp_param["value"] = value
        self._commit["set_remocomp_param"].append(set_remocomp_param)
        return self

    def set_remap(self, remap_from, remap_to, nsid=None):
        """
        @type remap_from: str
        @type remap_to: str
        @type nsid: str | None
        @rtype: ARemoConRelauncher
        """
        set_remap = dict()
        set_remap["from"] = remap_from
        set_remap["to"] = remap_to
        if nsid is not None:
            set_remap["nsid"] = nsid
        self._commit["set_remap"].append(set_remap)
        return self

    def set_remocomp_remap(self, remocomp_id, remap_from, remap_to, nsid=None):
        """
        @type remocomp_id: str
        @type remap_from: str
        @type remap_to: str
        @type nsid: str | None
        @rtype: ARemoConRelauncher
        """
        set_remocomp_remap = dict()
        set_remocomp_remap["id"] = remocomp_id
        set_remocomp_remap["from"] = remap_from
        set_remocomp_remap["to"] = remap_to
        if nsid is not None:
            set_remocomp_remap["nsid"] = nsid
        self._commit["set_remocomp_remap"].append(set_remocomp_remap)
        return self

    def set_all_to_initial_state(self):
        """
        @rtype: ARemoConRelauncher
        """
        self._clear()
        self._commit["set_all_to_initial_state"] = True
        return self

    def commit(self):
        """
        @rtype: None
        """
        try:
            self._commit_func(self._commit)
        finally:
            self._clear()

    def _clear(self):
        """
        @rtype: None
        """
        self._commit.pop("connect_to_master", None)
        self._commit.pop("connect_to_new_public_master", None)
        self._commit.pop("connect_to_new_private_master", None)
        self._commit["set_namespace"] = list()
        self._commit["set_type"] = list()
        self._commit["set_param"] = list()
        self._commit["set_remocomp_param"] = list()
        self._commit["set_remap"] = list()
        self._commit["set_remocomp_remap"] = list()
        self._commit["set_all_to_initial_state"] = False
