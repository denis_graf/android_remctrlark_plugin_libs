# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

import rcapy
from remctrlark.error_handling.handle_exception import handle_exception
from remctrlark.error_handling.RcaException import RcaException
from remctrlark.event_handling.RemoCompLaunchedEvent import RemoCompLaunchedEvent
from remctrlark.event_handling.RemoCoCustomEvent import RemoCoCustomEvent
from remctrlark.event_handling.Sl4aEventsListener import Sl4aEventsListener
from .ARemoCoScript import ARemoCoScript
from remctrlark.rca_proxy_wrappers.ProxyProxy import ProxyProxy
from remctrlark.rca_proxy_wrappers.RemoCoProxy import RemoCoProxy
from remctrlark.remocos.RemoConRelauncher import RemoConRelauncher
from remctrlark import utils


class RemoConScript(ARemoCoScript):
    class PyRemoConLayout(ARemoCoScript.PyRemoCoLayout, rcapy.APyRemoConLayout):
        def __init__(self, parent):
            """
            @type parent: RemoConScript
            """
            super(RemoConScript.PyRemoConLayout, self).__init__(parent)
            self._parent = parent
            self._parent._layout = self
            self._key_event_listeners = dict()

        def set_on_key_listener(self, actions, key_code, listener):
            """
            @type actions: list[int]
            @type key_code: int
            @type listener: None |
                            ( (str, dict[str, object]) -> None ) |
                            ( (object, str, dict[str, object]) -> None )
            """
            enable = listener is not None and len(actions) > 0
            self._parent._rpc_invoke_internal_method("layoutSetOnKeyListener", (actions, key_code, enable))
            if listener is None:
                del self._key_event_listeners[key_code]
            else:
                self._key_event_listeners[key_code] = listener

        def handle_key_event(self, name, data):
            """
            @type name: str
            @type data: dict[str, object]
            """
            assert name == "key"
            listener = self._key_event_listeners.get(data["key"])
            assert listener is not None
            listener(name, data)

    class RemoCompBinder(rcapy.ARemoCompBinder):
        def __init__(self, parent, proxy):
            """
            @type parent: RemoConScript
            @type proxy: RemoCoProxy
            """
            super(RemoConScript.RemoCompBinder, self).__init__()
            self._parent = parent
            self._proxy = proxy

        def __getattr__(self, name):
            """
            @type name: str
            @rtype: object
            """
            return self._proxy.rca.__getattr__(name)

    class RemoCompProxy(rcapy.ARemoCompProxy):
        def __init__(self, parent, remocomp_id):
            """
            @type remocomp_id: str
            @type parent: RemoConScript
            """
            super(RemoConScript.RemoCompProxy, self).__init__()
            self._parent = parent
            self._remocomp_id = remocomp_id
            self._proxy_out = RemoCoProxy(self._remocomp_id)

            self._proxy_in = None
            """@type: ProxyProxy"""
            self._binder = RemoConScript.RemoCompBinder(self._parent, self._proxy_out)
            self._remocomp_listener = None
            """@type: Sl4aEventsListener"""

            self._impl_events_listener = None
            self._impl_on_launched_listener = None

        def shutdown(self):
            self.remove_all_listeners()
            if self._proxy_out is not None:
                self._proxy_out.shutdown()
                self._proxy_out = None
                """@type: RemoCoProxy"""

        @property
        def the_type(self):
            """
            @rtype: str
            """
            # Note: The type can change every time by relaunching the remocomp, so we can't just cache it.
            return self._parent._rpc_invoke_internal_method("RemoCompProxy_getType", (self._remocomp_id,))

        @property
        def the_apk(self):
            """
            @rtype: str
            """
            # Note: The type can change every time by relaunching the remocomp, so we can't just cache it.
            return self._parent._rpc_invoke_internal_method("RemoCompProxy_getApk", (self._remocomp_id,))

        @property
        def the_id(self):
            """
            @rtype: str
            """
            return self._remocomp_id

        @property
        def binder(self):
            """
            @rtype: rcapy.ARemoCompBinder
            """
            return self._binder

        def set_events_listener(self, listener):
            """
            @type listener: None |
                            ( (rcapy.ARemoCompProxy, str, dict[str, object]) -> None ) |
                            ( (object, rcapy.ARemoCompProxy, str, dict[str, object]) -> None )
            """
            self._impl_events_listener = listener
            self._update()

        def set_on_launched_listener(self, listener):
            """
            @type listener: None |
                            ( (rcapy.ARemoCompProxy) -> None ) |
                            ( (object, rcapy.ARemoCompProxy -> None )
            """
            self._impl_on_launched_listener = listener
            self._update()

        def remove_all_listeners(self):
            self.set_events_listener(None)
            self.set_on_launched_listener(None)

        def _update(self):
            create_listener = self._impl_on_launched_listener is not None or self._impl_events_listener is not None
            listener_created = self._remocomp_listener is not None
            if create_listener == listener_created:
                return

            if create_listener:
                def event_handler(name, data):
                    """
                    @type name: str
                    @type data: dict[str, object]
                    """
                    if name == RemoCoCustomEvent.EVENT_NAME:
                        if self._impl_events_listener is not None:
                            def run():
                                hook = self._impl_events_listener
                                if hook is not None:
                                    custom_event = RemoCoCustomEvent(data)
                                    # noinspection PyCallingNonCallable
                                    hook(self, custom_event.name, custom_event.data)

                            self._parent.post_to_script_thread(run)
                    elif name == RemoCompLaunchedEvent.EVENT_NAME:
                        launched_event = RemoCompLaunchedEvent(data)
                        self._remocomp_type = launched_event.type
                        self._remocomp_apk = launched_event.apk
                        if self._impl_on_launched_listener is not None:
                            def run():
                                hook = self._impl_on_launched_listener
                                if hook is not None:
                                # noinspection PyCallingNonCallable
                                    hook(self)

                            self._parent.post_to_script_thread(run)
                    else:
                        self._parent._log.w("ignoring unknown event: {name: '%s', data: %s}" % (name, str(data)))

                self._proxy_in = ProxyProxy(self._proxy_out)
                self._remocomp_listener = \
                    Sl4aEventsListener(self._proxy_in, event_handler, handle_exception)
                self._remocomp_listener.start()
            else:
                if self._remocomp_listener is not None:
                    self._remocomp_listener.shutdown()
                    self._remocomp_listener = None
                    """@type: Sl4aEventsListener"""

                if self._proxy_in is not None:
                    self._proxy_in.shutdown()
                    self._proxy_in = None
                    """@type: ProxyProxy"""

    class PyRemoConConfig(ARemoCoScript.APyRemoCoConfig, rcapy.APyRemoConConfig):
        def __init__(self, parent):
            """
            @type parent: RemoConScript
            """
            super(RemoConScript.PyRemoConConfig, self).__init__(parent)

    class PyRemoConContext(ARemoCoScript.APyRemoCoContext, rcapy.APyRemoConContext):
        def __init__(self, parent):
            """
            @type parent: RemoConScript
            """
            super(RemoConScript.PyRemoConContext, self).__init__(parent)
            self._parent = parent
            self._layout = RemoConScript.PyRemoConLayout(self._parent)
            self._config = RemoConScript.PyRemoConConfig(self._parent)
            self._relauncher = RemoConRelauncher(self._relauncher_commit)

        @property
        def config(self):
            """
            @rtype: APyRemoConConfig
            """
            return self._config

        @property
        def layout(self):
            """
            @rtype: rcapy.APyRemoConLayout
            """
            return self._layout

        def get_remocomp_proxy(self, remocomp_id):
            """
            @type remocomp_id: str
            @rtype: rcapy.ARemoCompProxy
            """
            if remocomp_id in self._parent._remocomp_proxies:
                proxy = self._parent._remocomp_proxies[remocomp_id]
            else:
                proxy = RemoConScript.RemoCompProxy(self._parent, remocomp_id)
                self._parent._remocomp_proxies[remocomp_id] = proxy
            return proxy

        @property
        def relauncher(self):
            """
            @rtype: rcapy.ARemoConRelauncher
            """
            return self._relauncher

        def _relauncher_commit(self, commit):
            """
            @type commit: dict[str, object]
            """
            self._parent._rpc_invoke_internal_method("RemoConRelauncher_commit", (commit,))

    class PyRemoConConfiguration(ARemoCoScript.PyRemoCoConfiguration, rcapy.APyRemoConConfiguration):
        def __init__(self, parent):
            """
            @type parent: RemoConScript
            """
            super(RemoConScript.PyRemoConConfiguration, self).__init__(parent)
            self._parent = parent

    def __init__(self, proxy_in, script_id, script_file):
        """
        @type proxy_in: RemoCoScriptProxy
        @type script_id: str
        @type script_file: str
        """
        self._layout = None
        """@type: RemoConScript.PyRemoConLayout"""

        super(RemoConScript, self).__init__(proxy_in, script_id, script_file)
        self._remocomp_proxies = dict()
        self._remocon_context = RemoConScript.PyRemoConContext(self)
        self._remocon_impl = self.__create_new_remocon_impl()

    @property
    def context(self):
        """
        @rtype: rcapy.APyRemoConContext
        """
        return self._remocon_context

    @property
    def impl(self):
        """
        @rtype: rcapy.APyRemoConImpl
        """
        return self._get_remoco_impl()

    def _get_remoco_impl(self):
        """
        @rtype: rcapy.APyRemoConImpl
        """
        return self._remocon_impl

    def _remoco_create_binder(self):
        """
        @rtype: rcapy.AReflectingBinderImpl
        """
        return None

    def _create_internal_script_binder(self):
        """
        @rtype: rcapy.AReflectingBinderImpl | list | tuple | None
        """

        def remocon_handle_key_event(name, data):
            """
            @type name: str
            @type data: dict[str, object]
            """
            self.sync_run_on_script_thread(self.__remocon_handle_key_event, name, data)

        return super(RemoConScript, self)._create_internal_script_binder() + (remocon_handle_key_event,)

    def _remoco_start(self):
        self._remocon_impl.start(RemoConScript.PyRemoConConfiguration(self))

    def _on_finished(self):
        super(RemoConScript, self)._on_finished()
        for proxy in self._remocomp_proxies.values():
            assert isinstance(proxy, RemoConScript.RemoCompProxy)
            proxy.shutdown()
        self._remocomp_proxies.clear()

    def __create_new_remocon_impl(self):
        """
        @rtype: rcapy.APyRemoConImpl
        """
        assert not hasattr(self, "_remocon_impl")
        assert self._script_file is not None
        assert self._remocon_context is not None

        remocon_impl_module = utils.import_module(self._script_file)
        if remocon_impl_module is None:
            raise RcaException(
                "Python module '%s' for implementing RemoCon '%s' could not be loaded!" %
                (self._script_file, self.script_id))
        if hasattr(remocon_impl_module, 'Impl') and issubclass(remocon_impl_module.Impl, rcapy.APyRemoConImpl):
            remocon_impl = remocon_impl_module.Impl(self._remocon_context)
        else:
            raise RcaException(
                "Python module '%s' doesn't contain any implementation for RemoCon '%s'!" %
                (self._script_file, self.script_id))

        assert isinstance(remocon_impl, rcapy.APyRemoConImpl)
        return remocon_impl

    def __remocon_handle_key_event(self, name, data):
        """
        @type name: str
        @type data: dict[str, object]
        """
        assert isinstance(self._layout, RemoConScript.PyRemoConLayout)
        self._layout.handle_key_event(name, data)
