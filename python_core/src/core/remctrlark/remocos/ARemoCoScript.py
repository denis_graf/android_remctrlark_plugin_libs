# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from abc import abstractmethod, abstractproperty
import os
import pydoc
import threading

import rospy
import rospy.core

from remctrlark import utils
import rcapy
from remctrlark.binders.ARcaBinder import ARcaBinder
from remctrlark.error_handling.handle_exception import handle_exception
from remctrlark.event_handling.Sl4aEventsListener import Sl4aEventsListener
from remctrlark.rca_proxy_wrappers.AndroidProxy import AndroidProxy
from remctrlark.rca_proxy_wrappers.ProxyProxy import ProxyProxy
from remctrlark.rca_proxy_wrappers.RcaProxyWrapperLog import RcaProxyWrapperLog
from remctrlark.rca_script.RcaScript import RcaScript
from remctrlark.remocos.PyNodeConfiguration import PyNodeConfiguration


class ARemoCoScript(RcaScript):
    class ARemoCoLoadingTask(rcapy.ARemoCoLoadingTask):
        def __init__(self, parent, task_id):
            """
            @type parent: ARemoCoScript
            @type task_id: int
            """
            super(ARemoCoScript.ARemoCoLoadingTask, self).__init__()
            self._parent = parent
            self._task_id = task_id

        def done(self):
            """
            @rtype: None
            """
            self._parent._rpc_invoke_internal_method("RemoCoLoadingTask_done", (self._task_id,))

    class RemoCoLog(rcapy.ARemoCoLog):
        def __init__(self, script_id):
            """
            @type script_id: str
            """
            self._log = RcaProxyWrapperLog(script_id, True)

        def shutdown(self):
            self._log.shutdown()

        def e(self, msg):
            """
            @type msg: str
            """
            self._log.e(msg)

        def d(self, msg):
            """
            @type msg: str
            """
            self._log.d(msg)

        def i(self, msg):
            """
            @type msg: str
            """
            self._log.i(msg)

        def w(self, msg):
            """
            @type msg: str
            """
            self._log.w(msg)

    class Sl4aGroupView(rcapy.ASl4aGroupView):
        def __init__(self, parent):
            """
            @type parent: ARemoCoScript
            """
            super(ARemoCoScript.Sl4aGroupView, self).__init__()
            self._parent = parent
            self._event_listeners = dict()
            self._parent._layout_parent_view = self

        def query(self):
            """
            @rtype: dict[str, dict[str, str]]
            """
            return self._parent._rpc_invoke_internal_method("layoutQuery", ())

        def query_detail(self, view_id):
            """
            @type view_id: str
            @rtype: dict[str, str]
            """
            return self._parent._rpc_invoke_internal_method("layoutQueryDetail", (view_id,))

        def get_property(self, view_id, view_property):
            """
            @type view_id: str
            @type view_property: str
            @rtype object
            """
            return self._parent._rpc_invoke_internal_method("layoutGetProperty", (view_id, view_property))

        def set_property(self, view_id, view_property, value):
            """
            @type view_id: str
            @type view_property: str
            @type value: str
            """
            self._parent._rpc_invoke_internal_method("layoutSetProperty", (view_id, view_property, value))

        def set_list(self, view_id, values_list):
            """
            @type view_id: str
            @type values_list: str
            """
            self._parent._rpc_invoke_internal_method("layoutSetList", (view_id, values_list))

        def set_on_custom_event_listener(self, view_id, listener):
            """
            @type view_id: str
            @type listener: None |
                            ( (str, dict[str, object]) -> None ) |
                            ( (object, str, dict[str, object]) -> None )
            """
            enable = listener is not None
            self._parent._rpc_invoke_internal_method("layoutSetOnCustomEventListener", (view_id, enable))
            self._set_event_listener("custom", view_id, listener)

        def set_on_click_listener(self, view_id, listener):
            """
            @type view_id: str
            @type listener: None |
                            ( (str, dict[str, object]) -> None ) |
                            ( (object, str, dict[str, object]) -> None )
            """
            enable = listener is not None
            self._parent._rpc_invoke_internal_method("layoutSetOnClickListener", (view_id, enable))
            self._set_event_listener("click", view_id, listener)

        def set_on_item_click_listener(self, view_id, listener):
            """
            @type view_id: str
            @type listener: None |
                            ( (str, dict[str, object]) -> None ) |
                            ( (object, str, dict[str, object]) -> None )
            """
            enable = listener is not None
            self._parent._rpc_invoke_internal_method("layoutSetOnItemClickListener", (view_id, enable))
            self._set_event_listener("itemclick", view_id, listener)

        def set_on_touch_listener(self, actions, view_id, listener):
            """
            @type actions: list[int]
            @type view_id: str
            @type listener: None |
                            ( (str, dict[str, object]) -> None ) |
                            ( (object, str, dict[str, object]) -> None )
            """
            enable = listener is not None and len(actions) > 0
            self._parent._rpc_invoke_internal_method("layoutSetOnTouchListener", (actions, view_id, enable))
            self._set_event_listener("touch", view_id, listener)

        def call_method(self, view_id, method_name, args):
            """
            @type view_id: str
            @type method_name: str
            @type args: tuple
            @rtype object | None
            """
            return self._parent._rpc_invoke_internal_method("layoutCallMethod", (view_id, method_name, args))

        def call_method_on_ui_thread(self, view_id, method_name, args):
            """
            @type view_id: str
            @type method_name: str
            @type args: tuple
            @rtype object | None
            """
            return self._parent._rpc_invoke_internal_method("layoutCallMethodOnUiThread", (view_id, method_name, args))

        def handle_layout_event(self, name, data):
            """
            @type name: str
            @type data: dict[str, object]
            """
            listener = self._event_listeners.get((name, data["id"]))
            assert listener is not None
            listener(name, data)

        def on_recreate_layout(self):
            self._event_listeners = dict()

        def _set_event_listener(self, name, view_id, listener):
            """
            @type name: str
            @type view_id: str
            @type listener: None |
                            ( (str, dict[str, object]) -> None ) |
                            ( (object, str, dict[str, object]) -> None )
            """
            if listener is None:
                del self._event_listeners[name, view_id]
            else:
                self._event_listeners[name, view_id] = listener

    class PyRemoCoLayout(rcapy.APyRemoCoLayout):
        def __init__(self, parent):
            """
            @type parent: ARemoCoScript
            """
            super(ARemoCoScript.PyRemoCoLayout, self).__init__()
            self._parent = parent
            self._parent_view = ARemoCoScript.Sl4aGroupView(self._parent)

        def create_main(self):
            self._parent_view.on_recreate_layout()
            self._parent._rpc_invoke_internal_method("layoutCreateMain", ())

        def create_from_xml_string(self, xml):
            """
            @type xml: str
            """
            self._parent_view.on_recreate_layout()
            self._parent._rpc_invoke_internal_method("layoutCreateFromXmlString", (xml,))

        @property
        def parent_view(self):
            """
            @rtype: ASl4aGroupView
            """
            return self._parent_view

    class AndroidProxy(rcapy.AAndroidProxy):
        class Sl4aProxyBinder(ARcaBinder, rcapy.ASl4aProxyBinder):
            def __init__(self, sl4a):
                """
                @type sl4a: remctrlark.binders.Sl4aProxyBinder.Sl4aProxyBinder
                """
                super(ARemoCoScript.AndroidProxy.Sl4aProxyBinder, self).__init__()
                self._sl4a = sl4a

            def invoke_method(self, name, args):
                """
                @type name: str
                @type args: tuple
                @rtype: object
                """
                return self._sl4a.invoke_method(name, args)

            def get_methods(self):
                """
                @rtype: tuple[ARcaBinderMethod] | list[ARcaBinderMethod]
                """
                return rcapy.get_sl4a_methods()

            def _sl4a_rpc(self, method, *args):
                """
                @type method: str
                @type args: tuple
                @rtype: object | None
                """
                return self._sl4a.invoke_method(method, args)

        def __init__(self, parent):
            """
            @type parent: ARemoCoScript
            """
            super(ARemoCoScript.AndroidProxy, self).__init__()
            self._parent = parent
            self._proxy_out = AndroidProxy(parent.script_id)
            self._proxy_in = None
            """@type: ProxyProxy"""
            self._events_listener = None
            """@type: Sl4aEventsListener"""
            self._sl4a = ARemoCoScript.AndroidProxy.Sl4aProxyBinder(self._proxy_out.sl4a)

            self._parent._android_proxies.append(self)

        def shutdown(self):
            self.set_listener(None)
            if self._proxy_out is not None:
                self._proxy_out.shutdown()
                self._proxy_out = None
                self._sl4a = None
                """@type: remctrlark.rca_proxy_wrappers.AndroidProxy.AndroidProxy"""
                self._parent._android_proxies.remove(self)

        @property
        def sl4a(self):
            """
            @rtype: rcapy.ASl4aProxyBinder
            """
            return self._sl4a

        def set_listener(self, listener):
            """
            @type listener: None |
                            ( (rcapy.AAndroidProxy, str, dict[str, object]) -> None ) |
                            ( (object, rcapy.AAndroidProxy, str, dict[str, object]) -> None )
            """
            if self._sl4a is None:
                # The proxy is shutdown.
                return

            if self._events_listener is not None:
                self._events_listener.shutdown()
                self._events_listener = None
                """@type: Sl4aEventsListener"""

            if self._proxy_in is not None:
                self._proxy_in.shutdown()
                self._proxy_in = None
                """@type: ProxyProxy"""

            if listener is not None:
                def event_handler(name, data):
                    """
                    @type name: str
                    @type data: dict[str, object]
                    """

                    def run():
                        if self._events_listener is None:
                            return
                        # noinspection PyCallingNonCallable
                        listener(self, name, data)

                    self._parent.post_to_script_thread(run)

                self._proxy_in = ProxyProxy(self._proxy_out)
                self._events_listener = \
                    Sl4aEventsListener(self._proxy_in, event_handler, handle_exception)
                self._events_listener.start()

    # noinspection PyAbstractClass
    class APyRemoCoConfig(rcapy.APyRemoCoConfig):
        def __init__(self, parent):
            """
            @type parent: ARemoCoScript
            """
            super(ARemoCoScript.APyRemoCoConfig, self).__init__()
            self._parent = parent

        def get_params(self, namespace=None):
            """
            @type namespace: str | None
            @rtype: dict[str, object]
            """
            return self._parent._rpc_invoke_internal_method("getParams", (namespace,))

        def get_param(self, name, default_value=rcapy.APyRemoCoConfig._unspecified):
            """
            @type name: str
            @type default_value: str | int | float | bool
            @rtype: str | int | float | bool
            @raise RemoCoParamNotFoundException
            """
            value = self._parent._rpc_invoke_internal_method("getParam", (name,))
            if value is not None:
                return value
            if default_value != self._unspecified:
                return default_value
            raise rcapy.RemoCoParamNotFoundException(name)

    # noinspection PyAbstractClass
    class APyRemoCoContext(rcapy.APyRemoCoContext):
        def __init__(self, parent):
            """
            @type parent: ARemoCoScript
            """
            super(ARemoCoScript.APyRemoCoContext, self).__init__()
            self._parent = parent

        @property
        def the_id(self):
            """
            @rtype: str
            """
            return self._parent._remoco_id

        @property
        def the_type(self):
            """
            @rtype: str
            """
            return self._parent._remoco_type

        @property
        def the_apk(self):
            """
            @rtype: str
            """
            return self._parent._remoco_apk

        @property
        def namespace(self):
            """
            @rtype: str
            """
            return self._parent._rpc_invoke_internal_method("getNamespace", ())

        @property
        def log(self):
            """
            @rtype: ARemoCoLog
            """
            return self._parent._impl_log

        def handle_exception(self):
            """
            @rtype: None
            """
            handle_exception()

        def post_to_impl_thread(self, runnable):
            """
            @type runnable: ( () -> None ) | ( (object) -> None )
            @rtype: bool
            """

            def run():
                # noinspection PyBroadException
                try:
                    runnable()
                except:
                    handle_exception()

            return self._parent.post_to_script_thread(run)

        def run_on_impl_thread(self, runnable):
            """
            @type runnable: ( () -> None ) | ( (object) -> None )
            @rtype: bool
            """

            def run():
                # noinspection PyBroadException
                try:
                    runnable()
                except:
                    handle_exception()

            return self._parent.run_on_script_thread(run)

        def sync_run_on_impl_thread(self, runnable):
            """
            @type runnable: ( () -> None ) | ( (object) -> None )
            @rtype: bool
            """

            def run():
                """
                @rtype: bool
                """
                # noinspection PyBroadException
                try:
                    runnable()
                    return True
                except:
                    handle_exception()
                    return False

            return self._parent.sync_run_on_script_thread(run)

        def start_loading_task(self, msg):
            """
            @type msg: str
            @rtype: rcapy.ARemoCoLoadingTask | None
            """
            task = self._parent._rpc_invoke_internal_method("startLoadingTask", (msg,))
            """@type: dict[str]"""
            task_id = task["task_id"]
            """@type: int"""

            if task_id == 0:
                return None
            return ARemoCoScript.ARemoCoLoadingTask(self._parent, task_id)

        def create_android_proxy(self):
            """
            @rtype: rcapy.AAndroidProxy
            """
            return ARemoCoScript.AndroidProxy(self._parent)

        def request_remocon_reboot_on_relaunch(self, enable):
            """
            @type enable: bool
            @rtype: None
            """
            self._parent._rpc_invoke_internal_method("requestRemoConRebootOnRelaunch", (enable,))

        def is_on_shutdown(self):
            """
            @rtype: bool
            """
            return self._parent._rpc_invoke_internal_method("isOnShutdown", ())

        def is_on_relaunch(self):
            """
            @rtype: bool
            """
            return self._parent._rpc_invoke_internal_method("isOnRelaunch", ())

        def set_remocon_session_state(self, state):
            """
            @type state: object | None
            @rtype: None
            """
            self._parent._remocon_session_state = state
            self._parent._remocon_session_state_set = True

        def get_remocon_session_state(self):
            """
            @rtype: object | None
            """
            if not self._parent._remocon_session_state_set:
                self.set_remocon_session_state(self._parent._rpc_invoke_internal_method("getRemoConSessionState", ()))
            return self._parent._remocon_session_state

    class PyRemoCoConfiguration(rcapy.APyRemoCoConfiguration):
        def __init__(self, parent):
            """
            @type parent: ARemoCoScript
            """
            super(ARemoCoScript.PyRemoCoConfiguration, self).__init__()
            self._parent = parent

        def create_node_configuration(self, node_name):
            """
            @type node_name: str | None
            @rtype: tuple
            @return: Returns arguments tuple for calling rospy.init_node()
            """

            rpc_node_configuration = self._parent._rpc_invoke_internal_method("createNodeConfiguration", (node_name,))
            """@type: dict[str, object]"""
            node_configuration = PyNodeConfiguration(rpc_node_configuration)

            import sys

            if "-i" in sys.argv or "--determine-ros-ip" in sys.argv:
                # Try to determine the ip.
                # noinspection PyBroadException
                try:
                    import socket

                    sockconn = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

                    sockconn.connect((node_configuration.master_host, node_configuration.master_port))
                    sockname = sockconn.getsockname()
                    sockconn.close()
                    ip = sockname[0]
                    os.environ["ROS_IP"] = ip

                    if __debug__:
                        self._parent._log.i("determined ROS IP: " + ip)
                except:
                    # Let rospy decide what to do...
                    self._parent._log.i("no ROS IP could be determined")
                    os.environ["ROS_IP"] = node_configuration.ip
            else:
                os.environ["ROS_IP"] = node_configuration.ip

            os.environ["ROS_MASTER_URI"] = node_configuration.master_uri
            os.environ["ROS_NAMESPACE"] = node_configuration.namespace

            argv = set()
            remappings = node_configuration.remappings
            for remapping_key in remappings.keys():
                argv.add(str(remapping_key + ":=" + remappings[remapping_key]))

            # Note: Setting the environment variable ROS_NAMESPACE is to late here. Setting the namespace in node name
            # is not allowed. So we have to set the resolved name by setting the mapping of '__name'.
            if node_name is None:
                name = str(node_configuration.node_name)
            else:
                name = node_name
            assert len(node_configuration.namespace) > 0
            assert node_configuration.namespace[-1] != "/"
            argv.add(str("__name" + ":=" + node_configuration.namespace + "/" + name))

            anonymous = False
            log_level = None
            disable_rostime = False
            disable_rosout = False

            # Issue #30: ipython interface:
            # To run in ipython we have to start the script in an own thread, but ROS signals can be set only when
            # calling rospy.init_node() on the main thread.
            disable_signals = not threading.current_thread().getName() == "MainThread"

            xmlrpc_port = 0
            tcpros_port = 0

            # Note: name and argv should contain strings of type str, not unicode. Otherwise there can arrive problems,
            # e.g. when calling remapped services.
            conf = name, argv, anonymous, log_level, disable_rostime, disable_rosout, disable_signals, \
                xmlrpc_port, tcpros_port

            self._parent._log.i("created ROS node configuration:\n"
                                "  ROS_IP=%s\n"
                                "  ROS_MASTER_URI=%s\n"
                                "  ROS_NAMESPACE=%s\n"
                                "  rospy.init_node%s" % (
                                    os.getenv("ROS_IP"),
                                    os.getenv("ROS_MASTER_URI"),
                                    os.getenv("ROS_NAMESPACE"),
                                    str(conf)))

            return conf

    def __init__(self, proxy, script_id, script_file):
        """
        @type proxy: RemoCoScriptProxy
        @type script_id: str
        @type script_file: str
        """
        self._script_file = script_file
        self._impl_log = ARemoCoScript.RemoCoLog(script_id)
        self._layout_parent_view = None
        """@type: ARemoCoScript.Sl4aGroupView"""
        self._android_proxies = list()
        """@type: list[AndroidProxy]"""

        super(ARemoCoScript, self).__init__(proxy, script_id)
        self._impl_doc = None

        remoco_info = proxy.get_script_info()
        self._remoco_type = remoco_info["type"]
        self._remoco_apk = remoco_info["apk"]
        self._remoco_id = script_id

        self.__remoco_start_requested = False
        self.__remoco_shutdown_requested = False

        self._remocon_session_state_set = False
        self._remocon_session_state = None

    # noinspection PyPropertyDefinition
    @abstractproperty
    @property
    def context(self):
        """
        This is useful in ipython. (Issue #30: ipython interface)
        @rtype: rcapy.APyRemoCoContext
        """

    # noinspection PyPropertyDefinition
    @abstractproperty
    @property
    def impl(self):
        """
        This is useful in ipython. (Issue #30: ipython interface)
        @rtype: rcapy.APyRemoCoImpl
        """

    @abstractmethod
    def _get_remoco_impl(self):
        """
        @rtype: rcapy.APyRemoCoImpl
        """

    @abstractmethod
    def _remoco_create_binder(self):
        """
        @rtype: rcapy.AReflectingBinderImpl
        """

    @abstractmethod
    def _remoco_start(self):
        pass

    def _create_impl_doc(self):
        return utils.stripbs(pydoc.getdoc(self._get_remoco_impl().__class__))

    def _remoco_shutdown(self):
        self._get_remoco_impl().shutdown()

    def _remoco_create_ui(self):
        self._get_remoco_impl().create_ui()

    def _create_public_script_binder(self):
        """
        @rtype: rcapy.AReflectingBinderImpl
        """
        return self._remoco_create_binder()

    def _create_internal_script_binder(self):
        """
        @rtype: rcapy.AReflectingBinderImpl | list | tuple | None
        """

        def get_remoco_doc():
            """
            @rtype: str
            """
            if self._impl_doc is None:
                self._impl_doc = self._create_impl_doc()
            return self._impl_doc

        def remoco_start():
            self.sync_run_on_script_thread(self.__remoco_start)

        def remoco_shutdown():
            def run():
                self.__remoco_shutdown()
                self.shutdown()

            self.sync_run_on_script_thread(run)

        def remoco_create_ui():
            self.sync_run_on_script_thread(self.__remoco_create_ui)

        def remoco_handle_layout_event(name, data):
            """
            @type name: str
            @type data: dict[str, object]
            """
            self.sync_run_on_script_thread(self.__remoco_handle_layout_event, name, data)

        return get_remoco_doc, remoco_start, remoco_shutdown, remoco_create_ui, remoco_handle_layout_event

    def _on_finished(self):
        self.__remoco_shutdown()

        if self._remocon_session_state_set:
            self._rpc_invoke_internal_method("setRemoConSessionState", (self._remocon_session_state,))
            self._remocon_session_state_set = False

        android_proxies = list(self._android_proxies)
        for proxy in android_proxies:
            assert isinstance(proxy, ARemoCoScript.AndroidProxy)
            proxy.shutdown()
        assert len(self._android_proxies) == 0

        if self._impl_log is not None:
            self._impl_log.shutdown()
            self._impl_log = None
            """@type: ARemoCoScript.RemoCoLog"""

        super(ARemoCoScript, self)._on_finished()

    def __remoco_start(self):
        assert not self.__remoco_start_requested
        self.__remoco_start_requested = True
        self._log.i(ARemoCoScript.__remoco_start.__name__)

        # Register a shutdown hook which shuts down the script, when ROS is shut down.
        def shutdown_hook():
            self.shutdown()

        rospy.on_shutdown(shutdown_hook)

        self._remoco_start()

    def __remoco_shutdown(self):
        if self.__remoco_shutdown_requested:
            return
        self.__remoco_shutdown_requested = True
        if not self.__remoco_start_requested:
            return
        self._log.i(ARemoCoScript.__remoco_shutdown.__name__)

        self._remoco_shutdown()

        # Note: We have to call rospy.signal_shutdown() here for unregistering the ROS node by rospy.
        rospy.signal_shutdown("Requested by Python...")

    def __remoco_create_ui(self):
        self._remoco_create_ui()

    def __remoco_handle_layout_event(self, name, data):
        """
        @type name: str
        @type data: dict[str, object]
        """
        assert isinstance(self._layout_parent_view, ARemoCoScript.Sl4aGroupView)
        self._layout_parent_view.handle_layout_event(name, data)
