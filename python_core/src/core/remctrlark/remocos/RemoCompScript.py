# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from remctrlark import utils
import rcapy
from .ARemoCoScript import ARemoCoScript
from remctrlark.error_handling.RcaException import RcaException


class RemoCompScript(ARemoCoScript):
    class PyRemoCompLayout(ARemoCoScript.PyRemoCoLayout, rcapy.APyRemoCompLayout):
        def __init__(self, parent):
            """
            @type parent: RemoCompScript
            """
            super(RemoCompScript.PyRemoCompLayout, self).__init__(parent)
            self._parent = parent

    class PyRemoCompEventDispatcher(rcapy.APyRemoCompEventDispatcher):
        def __init__(self, parent):
            """
            @type parent: RemoCompScript
            """
            super(RemoCompScript.PyRemoCompEventDispatcher, self).__init__()
            self._parent = parent

        def dispatch(self, name, data):
            """
            @type name: str
            @type data: dict[str, object]
            """
            self._parent._rpc_invoke_internal_method("RemoCompEventDispatcher_dispatch", (name, data))

    class PyRemoCompConfig(ARemoCoScript.APyRemoCoConfig, rcapy.APyRemoCompConfig):
        def __init__(self, parent):
            """
            @type parent: RemoCompScript
            """
            super(RemoCompScript.PyRemoCompConfig, self).__init__(parent)

    class PyRemoCompContext(ARemoCoScript.APyRemoCoContext, rcapy.APyRemoCompContext):
        def __init__(self, parent):
            """
            @type parent: RemoCompScript
            """
            super(RemoCompScript.PyRemoCompContext, self).__init__(parent)
            self._parent = parent
            self._root_view = RemoCompScript.PyRemoCompLayout(self._parent)
            self._config = RemoCompScript.PyRemoCompConfig(self._parent)

        @property
        def config(self):
            """
            @rtype: APyRemoCompConfig
            """
            return self._config

        @property
        def layout(self):
            """
            @rtype: rcapy.APyRemoCompLayout
            """
            return self._root_view

        @property
        def event_dispatcher(self):
            """
            @rtype: rcapy.APyRemoCompEventDispatcher
            """
            return self._parent._event_dispatcher

    class PyRemoCompConfiguration(ARemoCoScript.PyRemoCoConfiguration, rcapy.APyRemoCompConfiguration):
        def __init__(self, parent):
            """
            @type parent: RemoCompScript
            """
            super(RemoCompScript.PyRemoCompConfiguration, self).__init__(parent)
            self._parent = parent

    def __init__(self, proxy, script_id, script_file):
        """
        @type proxy: RemoCoScriptProxy
        @type script_id: str
        @type script_file: str
        """
        super(RemoCompScript, self).__init__(proxy, script_id, script_file)
        self._event_dispatcher = RemoCompScript.PyRemoCompEventDispatcher(self)
        self._remocomp_context = RemoCompScript.PyRemoCompContext(self)
        self._remocomp_impl = self.__create_new_remocomp_impl()

    @property
    def context(self):
        """
        @rtype: rcapy.APyRemoCompContext
        """
        return self._remocomp_context

    @property
    def impl(self):
        """
        @rtype: rcapy.APyRemoCompImpl
        """
        return self._get_remoco_impl()

    def _get_remoco_impl(self):
        """
        @rtype: rcapy.APyRemoCompImpl
        """
        return self._remocomp_impl

    def _remoco_create_binder(self):
        """
        @rtype: rcapy.AReflectingBinderImpl
        """
        return self._remocomp_impl.create_binder()

    def _remoco_start(self):
        self._remocomp_impl.start(RemoCompScript.PyRemoCompConfiguration(self))

    def __create_new_remocomp_impl(self):
        """
        @rtype: rcapy.APyRemoCompImpl
        """
        assert not hasattr(self, "_remocomp_impl")
        assert self._script_file is not None
        assert self._remocomp_context is not None

        remocomp_impl_module = utils.import_module(self._script_file)
        if remocomp_impl_module is None:
            raise RcaException(
                "Python module '%s' for implementing RemoComp '%s' could not be loaded!" %
                (self._script_file, self.script_id))
        if hasattr(remocomp_impl_module, 'Impl') and issubclass(remocomp_impl_module.Impl, rcapy.APyRemoCompImpl):
            remocomp_impl = remocomp_impl_module.Impl(self._remocomp_context)
        else:
            raise RcaException(
                "Python module '%s' doesn't contain any implementation for RemoComp '%s'!" %
                (self._script_file, self.script_id))

        assert isinstance(remocomp_impl, rcapy.APyRemoCompImpl)
        return remocomp_impl
