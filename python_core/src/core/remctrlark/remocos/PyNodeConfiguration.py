# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.


class PyNodeConfiguration(object):
    def __init__(self, node_configuration):
        """
        @type node_configuration: dict[str, object]
        """
        super(PyNodeConfiguration, self).__init__()

        self._master_uri = node_configuration["master_uri"]
        """@type: str"""
        assert isinstance(self._master_uri, basestring)

        self._master_host = node_configuration["master_host"]
        """@type: str"""
        assert isinstance(self._master_host, basestring)

        self._master_port = node_configuration["master_port"]
        """@type: int"""
        assert isinstance(self._master_port, int)

        self._ip = node_configuration["ip"]
        """@type: str"""
        assert isinstance(self._ip, basestring)

        self._node_name = node_configuration["node_name"]
        """@type: str"""
        assert isinstance(self._node_name, basestring)

        self._namespace = node_configuration["namespace"]
        """@type: str"""
        assert isinstance(self._namespace, basestring)

        self._remappings = node_configuration["remappings"]
        """@type: dict[str, str]"""
        assert isinstance(self._remappings, dict)

    @property
    def master_uri(self):
        """
        @rtype: str
        """
        return self._master_uri

    @property
    def master_host(self):
        """
        @rtype: str
        """
        return self._master_host

    @property
    def master_port(self):
        """
        @rtype: int
        """
        return self._master_port

    @property
    def ip(self):
        """
        @rtype: str
        """
        return self._ip

    @property
    def node_name(self):
        """
        @rtype: str
        """
        return self._node_name

    @property
    def namespace(self):
        """
        @rtype: str
        """
        return self._namespace

    @property
    def remappings(self):
        """
        @rtype: dict[str, str]
        """
        return self._remappings
