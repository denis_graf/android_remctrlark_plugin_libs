# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

import socket
import threading
import traceback
import sys

from remctrlark.rca_logging.RcaGlobalLog import RcaGlobalLog


# TODO: handle uncaught exceptions in sub threads/precesses:
# implement a wrapper function for wrapping callback function that are called in sub threads/precesses
def handle_exception(send_report=True):
    """
    Handles the last exception communicating it to RemCtrlArk via ErrorProxy. It returns the handled exception.
    @type send_report: bool
    @rtype: dict[str, object]
    """
    # noinspection PyBroadException
    log = RcaGlobalLog("handle_exception")

    # noinspection PyBroadException
    try:
        hostname = socket.gethostname()
    except:
        hostname = None

    if hostname is None:
        log.e_exc("Uncaught exception on unkown host...")
    else:
        log.e_exc("Uncaught exception on host '%s'..." % hostname)

    timeout_duration = 1  # Wait for 1 second.

    exc_info = get_exc_info()

    if send_report:
        _try_send_report(exc_info, log, timeout_duration)

    return exc_info


def _try_send_report(exc_info, log, timeout_duration):
    """
    @type exc_info: dict[str, object]
    @type log: ARcaLog
    @type timeout_duration: int
    """
    thread = threading.Thread(target=_send_report, args=(exc_info, log))
    thread.setDaemon(True)
    thread.start()
    thread.join(timeout_duration)
    if thread.isAlive():
        # Thread is hanging, and we can't stop it... The main thread should be forced to stop!
        log.e("Timeout on sending python exception information to RemCtrlArk!")


def _send_report(exc_info, log):
    """
    @type exc_info: dict[str, object]
    @type log: ARcaLog
    """
    try:
        from remctrlark.rca_proxy_wrappers.ErrorProxy import ErrorProxy

        with ErrorProxy() as proxy:
            proxy.rca_send_error_report(exc_info)
    except Exception as e:
        log.e("Error on sending exception information to RemCtrlArk: %s%s" % (e.__class__.__name__, str(e)))


def get_exc_info():
    return _get_exc_info(*sys.exc_info())


def _get_exc_info(exc_type, exc_value, exc_traceback):
    """
    @return: dict[str, object]
    """
    from rcapy import RcaPyException
    from remctrlark.error_handling.RcaException import RcaException

    if isinstance(exc_value, dict):
        return exc_value

    exc_info = dict()

    exc_info["class_name"] = exc_value.__class__.__name__
    exc_info["str"] = str(exc_value)

    if isinstance(exc_value, Exception):
        exc_info["message"] = exc_value.message
    else:
        exc_info["message"] = None

    if isinstance(exc_value, RcaException) or isinstance(exc_value, RcaPyException) and exc_value.cause is not None:
        exc_info["cause"] = _get_exc_info(None, exc_value.cause, None)
    else:
        exc_info["cause"] = None

    if exc_traceback is not None and exc_type is not None:
        exc_info["traceback"] = "".join(traceback.format_exception(exc_type, exc_value, exc_traceback))
    else:
        exc_info["traceback"] = None

    return exc_info
