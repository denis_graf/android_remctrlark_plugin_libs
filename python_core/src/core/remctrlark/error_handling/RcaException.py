# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.


class RcaException(Exception):
    def __init__(self, message, cause=None):
        """
        @type message: str
        @type cause: object | dict | None
        """
        super(RcaException, self).__init__()
        self._message = message
        self._cause = cause

    @property
    def message(self):
        """
        @return: str
        """
        return self._message

    @property
    def cause(self):
        """
        @return: object | None
        """
        return self._cause

    def __str__(self):
        result = self._message

        done = False
        if isinstance(self._cause, dict):
            # noinspection PyBroadException
            try:
                result += "\n  Caused by: " + self._cause["str"]
                done = True
            except:
                pass

        if not done and self._cause is not None:
            result += "\n  Caused by: " + str(self._cause)

        return result
