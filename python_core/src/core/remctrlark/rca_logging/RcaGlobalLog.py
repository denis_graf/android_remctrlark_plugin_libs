# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

import multiprocessing

from .ARcaLog import ARcaLog


_logger = multiprocessing.log_to_stderr()


class RcaGlobalLog(ARcaLog):

    def __init__(self, tag):
        """
        @type tag: str | None
        """
        super(RcaGlobalLog, self).__init__(tag)

    @staticmethod
    def set_level(level):
        """
        @param level: One of logging.CRITICAL, .FATAL, .ERROR, .WARNING, .WARN, .INFO, .DEBUG, .NOTSET
        """
        global _logger
        _logger.setLevel(level)

    def e(self, msg):
        """
        @type msg: str
        """
        _logger.error(self._append_tag(msg))

    def e_exc(self, msg):
        """
        @type msg: str
        """
        _logger.error(self._append_tag(msg), exc_info=1)

    def d(self, msg):
        """
        @type msg: str
        """
        if __debug__:
            _logger.debug(self._append_tag(msg))

    def i(self, msg):
        """
        @type msg: str
        """
        _logger.info(self._append_tag(msg))

    def w(self, msg):
        """
        @type msg: str
        """
        _logger.warning(self._append_tag(msg))
