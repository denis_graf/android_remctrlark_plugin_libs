# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from abc import ABCMeta, abstractmethod


class ARcaLog(object):
    __metaclass__ = ABCMeta

    def __init__(self, tag):
        """
        @type tag: str | None
        """
        super(ARcaLog, self).__init__()
        if tag is None:
            self._tag = ""
        else:
            self._tag = "[%s] " % tag

    @property
    def tag(self):
        """
        @rtype: str
        """
        return self._tag

    def _append_tag(self, msg):
        """
        @type msg: str
        """
        return self._tag + msg

    @abstractmethod
    def e(self, msg):
        """
        @type msg: str
        """

    @abstractmethod
    def e_exc(self, msg):
        """
        @type msg: str
        """

    @abstractmethod
    def d(self, msg):
        """
        @type msg: str
        """

    @abstractmethod
    def i(self, msg):
        """
        @type msg: str
        """

    @abstractmethod
    def w(self, msg):
        """
        @type msg: str
        """