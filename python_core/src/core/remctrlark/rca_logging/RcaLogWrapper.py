# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from .ARcaLog import ARcaLog


class RcaLogWrapper(ARcaLog):
    def __init__(self, log, tag=None):
        """
        @type log: ARcaLog
        @type tag: str
        """
        super(RcaLogWrapper, self).__init__(tag)
        self._log = log

    def _append_tag(self, msg):
        return self._log._append_tag(self._super_append_tag(msg))

    def _super_append_tag(self, msg):
        return super(RcaLogWrapper, self)._append_tag(msg)

    def e(self, msg):
        """
        @type msg: str
        """
        self._log.e(self._super_append_tag(msg))

    def e_exc(self, msg):
        """
        @type msg: str
        """
        self._log.e_exc(self._super_append_tag(msg), exc_info=1)

    def d(self, msg):
        """
        @type msg: str
        """
        if __debug__:
            self._log.d(self._super_append_tag(msg))

    def i(self, msg):
        """
        @type msg: str
        """
        self._log.i(self._super_append_tag(msg))

    def w(self, msg):
        """
        @type msg: str
        """
        self._log.w(self._super_append_tag(msg))
