# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.
from _socket import timeout

from abc import abstractmethod
import os
import socket
import threading

from .ASl4aProxy import ASl4aProxy
from remctrlark.error_handling.RcaException import RcaException
from remctrlark.event_handling.RcaEvent import RcaEvent


_RCA_EVENT_INTERRUPT = "rca.event.interrupt"

# TODO: complete missing type hinting!


class ARcaProxy(ASl4aProxy):
    def __init__(self, type_name, addr=None):
        """
        @type type_name: str
        @param addr: tuple | None
        """
        super(ARcaProxy, self).__init__(addr)
        self.__type_name = type_name
        self.__hostname = socket.gethostname()
        self.__pid = os.getpid()
        self.__tid = threading.current_thread().ident
        self.__rid = self._rca_init(self.rca_type_name, self.rca_hostname, self.rca_pid, self.rca_tid)
        self.__interrupt_wait_for_event = False

    def _rca_init(self, type_name, hostname, pid, tid):
        """
        @type type_name: str
        @type hostname: str
        @type pid: int
        @type tid: int
        @rtype: int
        """
        return self._rcaInit(type_name, hostname, pid, tid)

    @abstractmethod
    def sl4a_request_access_to_blocked_proxy(self):
        pass

    @abstractmethod
    def sl4a_debug_on_blocked_proxy(self, method, args):
        """
        @type method: str
        @type args: tuple
        """

    @property
    def rca_rid(self):
        """
        @rtype: int
        """
        return self.__rid

    @property
    def rca_type_name(self):
        """
        @rtype: str
        """
        return self.__type_name

    @property
    def rca_hostname(self):
        """
        @rtype: str
        """
        return self.__hostname

    @property
    def rca_pid(self):
        """ Process id.
        @rtype: int
        """
        return self.__pid

    @property
    def rca_tid(self):
        """ Thread id.
        @rtype: int
        """
        return self.__tid

    def rca_bind_as_proxy(self, proxy_rid):
        """
        @type proxy_rid: int
        """
        return self._rcaBindAsProxy(proxy_rid)

    def rca_bind_as_android(self, remoco_id):
        return self._rcaBindAsAndroid(remoco_id)

    def rca_bind_as_ipyquery(self):
        return self._rcaBindAsIPyQuery()

    def rca_bind_as_error(self):
        return self._rcaBindAsError()

    def rca_bind_as_script_process_messenger(self):
        return self._rcaBindAsScriptProcessMessenger()

    def rca_bind_as_interrupter(self):
        return self._rcaBindAsInterrupter()

    def rca_bind_as_logger(self, is_remoco_impl_log, tag):
        """
        @type is_remoco_impl_log: bool
        @type tag: str
        @rtype: None
        """
        self._rcaBindAsLogger(is_remoco_impl_log, tag)

    def rca_bind_as_main_script(self):
        """
        @rtype name: str
        """
        return self._rcaBindAsMainScript()

    def rca_bind_as_remoco_script(self, name):
        """
        @type name: str
        """
        return self._rcaBindAsRemoCoScript(name)

    def rca_bind_as_script_binder_server(self, name, methods_info):
        """
        @type name: str
        @type methods_info: list[dict[str, object]]
        """
        return self._rcaBindAsScriptBinderServer(name, methods_info)

    def rca_bind_as_remoco(self, name):
        """
        @type name: str
        """
        return self._rcaBindAsRemoCo(name)

    def rca_call_method(self, name, args):
        """
        @type name: str
        @type args: tuple
        """
        return self._rcaCallMethod(name, args)

    def rca_wait_for_event(self):
        """
        @rtype: RcaEvent | None
        """
        # Note: For simplicity we use the SL4A event method.
        try:
            event = RcaEvent(self.eventWait())
        except RcaException as e:
            if isinstance(e.cause, timeout):
                return None
            else:
                raise e
        except timeout:
            return None
        if event.name == _RCA_EVENT_INTERRUPT:
            return None
        return event

    def rca_interrupt_wait_for_event(self):
        from remctrlark.rca_proxy_wrappers.InterrupterProxy import InterrupterProxy

        with InterrupterProxy() as proxy:
            proxy.rca_send_interrupt_event(self.rca_rid)
