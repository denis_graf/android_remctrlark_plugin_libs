# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from abc import abstractmethod, ABCMeta
import socket
from threading import RLock
import os

import android
from remctrlark.error_handling.RcaException import RcaException


android.PORT = os.environ.get('RCA_AP_PORT')
android.HOST = os.environ.get('RCA_AP_HOST')
android.HANDSHAKE = os.environ.get('RCA_AP_HANDSHAKE')


class ASl4aProxy(android.Android):
    __metaclass__ = ABCMeta

    def __init__(self, addr=None):
        self.__lock1 = RLock()
        self.__lock2 = RLock()
        self.__lock3 = RLock()
        self.__locked = 0
        try:
            super(ASl4aProxy, self).__init__(addr)
        except socket.error as e:
            raise RcaException("Could not establish connection to RemCtrlArk-App!", e)

    @abstractmethod
    def sl4a_request_access_to_blocked_proxy(self):
        pass

    @abstractmethod
    def sl4a_debug_on_blocked_proxy(self, method, args):
        """
        @type method: str
        @type args: tuple
        """

    def sl4a_shutdown(self):
        if self.client is not None:
            self.client.close()
            self.client = None
            """@type: socket._fileobject"""
        if self.conn is not None:
            self.conn.close()
            self.conn = None
            """@type: SocketType"""

    def sl4a_call_method(self, name, args):
        """
        @type name: str
        @type args: tuple
        @rtype: object
        """
        return self._rpc(name, *args)

    def _rpc_synchronized(self, method, *args):
        """ Returns instance of collections.namedtuple('Result', 'id,result,error')
        @type method: str
        @type args: tuple
        """
        error = False
        result = None

        if __debug__ and self.__locked > 0:
            self.sl4a_debug_on_blocked_proxy(method, args)

        # State 0: At this point, any amount of other threads can get access to proxy before this thread.
        self.__lock1.acquire()
        try:
            # State 1: Here arrived, this thread will be the next one getting access to proxy. Try to get access.
            if self.__locked > 0:
                # The proxy is blocked. Try to get access.
                self.sl4a_request_access_to_blocked_proxy()
        except:
            error = True
            raise
        finally:
            if error:
                self.__lock1.release()
            else:
                with self.__lock2:
                    # State 2: Wait for access to proxy.
                    self.__locked += 1
                    self.__lock1.release()
                    self.__lock3.acquire()

                # State 3: Got access to proxy, execute RPC.
                try:
                    result = super(ASl4aProxy, self)._rpc(method, *args)
                except Exception as e:
                    raise RcaException("RPC error: %s%s" % (method, args), e)
                finally:
                    self.__locked -= 1
                    self.__lock3.release()

        return result

    def _rpc(self, method, *args):
        """
        @type method: str
        @type args: tuple
        @rtype: object
        """
        result = self._rpc_synchronized(method, *args)

        if result.error is not None:
            raise RcaException(result.error)

        return result.result
