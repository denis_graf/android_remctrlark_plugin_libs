# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

import os
import imp
import sys


def import_module(path_to_file):
    """
    Imports a module from given file and adds the module's path to os.path.
    @param path_to_file: Path to module's .py file to be loaded. The file extension has not to be set. If path is
    relative, it is resolved in respect to current working directory.
    @type path_to_file: str
    @rtype: module
    """
    full_path_to_file = os.path.normpath(os.path.join(os.getcwd(), path_to_file))
    path, fname = os.path.split(full_path_to_file)
    mname, ext = os.path.splitext(fname)

    sys.path.append(path)

    no_ext = os.path.join(path, mname)
    module_file = no_ext + '.py'
    if os.path.exists(module_file):
        return imp.load_source(mname, module_file)


def stripbs(sin):
    """
    Processes all \b.
    Source: http://www.gossamer-threads.com/lists/python/python/1291
    @type sin: str
    @rtype: str
    """
    import string

    sout = []
    for ch in sin:
        if ch == '\b':
            del sout[-1:]  # a nop if len(sout) == 0
        else:
            sout.append(ch)
    return string.join(sout, '')
