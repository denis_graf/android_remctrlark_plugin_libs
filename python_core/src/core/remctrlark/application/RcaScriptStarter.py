# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

import os
import sys
from remctrlark.error_handling.RcaException import RcaException


class RcaScriptStarter(object):
    def __init__(self):
        super(RcaScriptStarter, self).__init__()
        self._script_proxy = None
        """@type: RemoCoScriptProxy"""
        self._script = None
        """@type: ARcaScript"""

    @property
    def script(self):
        """
        This is useful in ipython. (Issue #30: ipython interface)
        @rtype: ARcaScript
        """
        return self._script

    def start_as_subprocess(self, script_id, on_created=None, on_started=None, on_exited=None, exception_handler=None):
        """ Starts the requested script as a new subprocess.
        @type script_id: str
        @type on_created: (RcaScript) -> None | None
        @type on_started: () -> None | None
        @type on_exited: () -> None | None
        @type exception_handler: ( () -> object ) or ( (object) -> object ) | None
        @rtype: Process
        """
        from multiprocessing import Process

        subprocess = Process(target=self.start, args=(script_id, on_created, on_started, on_exited, exception_handler))
        subprocess.start()
        return subprocess

    def start_in_new_thread(self, script_id, on_created=None, on_started=None, on_exited=None, exception_handler=None):
        """ Starts the requested script as a new subprocess.
        This has to be used in ipython. (Issue #30: ipython interface)
        @type script_id: str
        @type on_created: (RcaScript) -> None | None
        @type on_started: () -> None | None
        @type on_exited: () -> None | None
        @type exception_handler: ( () -> object ) or ( (object) -> object ) | None
        @rtype: Thread
        """
        from threading import Thread

        new_thread = Thread(target=self.start, args=(script_id, on_created, on_started, on_exited, exception_handler))
        new_thread.start()
        return new_thread

    def start(self, script_id, on_created=None, on_started=None, on_exited=None, exception_handler=None):
        """ Starts the requested script in current process.
        @type script_id: str
        @type on_created: (RcaScript) -> None | None
        @type on_started: () -> None | None
        @type on_exited: () -> None | None
        @type exception_handler: ( () -> object ) or ( (object) -> object ) | None
        """
        # noinspection PyBroadException
        try:
            self._start(script_id, on_created, on_started, on_exited)
        except:
            if exception_handler is None:
                raise
            else:
                # noinspection PyCallingNonCallable
                exception_handler()

    def _start(self, script_id, on_created=None, on_started=None, on_exited=None):
        """
        @type script_id: str
        @type on_created: (RcaScript) -> None
        @type on_started: () -> None
        @type on_exited: () -> None | None
        """
        from remctrlark.rca_script.MainScript import MainScript
        if script_id.startswith(MainScript.SCRIPT_ID):
            self._script = MainScript()
        else:
            from remctrlark.rca_proxy_wrappers.RemoCoScriptProxy import RemoCoScriptProxy

            self._script_proxy = RemoCoScriptProxy(script_id)
            script_info = self._script_proxy.get_script_info()
            apk_dir = os.path.join("..", script_info["apk"])

            script_id = script_info["id"]
            """@type: str"""
            script_file = os.path.join(apk_dir, script_info["script"])
            """@type: str"""
            script_type = script_info['super_type']
            """@type: str"""

            sys.path.append(os.path.realpath(os.path.join(apk_dir, "shared")))
            os.chdir(os.path.dirname(os.path.realpath(script_file)))
            script_file = os.path.basename(script_file)

            if script_type == 'remocon':
                from remctrlark.remocos.RemoConScript import RemoConScript

                self._script = RemoConScript(self._script_proxy, script_id, script_file)
            elif script_type == 'remocomp':
                from remctrlark.remocos.RemoCompScript import RemoCompScript

                self._script = RemoCompScript(self._script_proxy, script_id, script_file)
            else:
                raise RcaException("Unknown super type for script id '%s'!" % script_id)

        if on_created is not None:
            on_created(self._script)
        if on_started is not None:
            self._script.post_to_script_thread(on_started)

        self._script.start()

        if on_exited is not None:
            on_exited()


def start_script_as_subprocess(script_id, on_created=None, on_started=None, on_exited=None, exception_handler=None):
    """ Launches the requested script as a subprocess.
    @type script_id: str
    @type on_created: (RcaScript) -> None | None
    @type on_started: () -> None | None
    @type on_exited: () -> None | None
    @type exception_handler: ( () -> object ) or ( (object) -> object ) | None
    @rtype: Process
    """
    return RcaScriptStarter().start_as_subprocess(script_id, on_created, on_started, on_exited, exception_handler)


def start_script_in_new_thread(script_id, on_created=None, on_started=None, on_exited=None, exception_handler=None):
    """ Launches the requested script in a new thread.
    @type script_id: str
    @type on_created: (RcaScript) -> None | None
    @type on_started: () -> None | None
    @type on_exited: () -> None | None
    @type exception_handler: ( () -> object ) or ( (object) -> object ) | None
    @rtype: Thread
    """
    return RcaScriptStarter().start_in_new_thread(script_id, on_created, on_started, on_exited, exception_handler)


def start_script(script_id, on_created=None, on_started=None, on_exited=None, exception_handler=None):
    """ Launches the requested script in current thread.
    @type on_created: (RcaScript) -> None | None
    @type on_started: () -> None | None
    @type on_exited: () -> None | None
    @type exception_handler: ( () -> object ) or ( (object) -> object ) | None
    @type script_id: str
    """
    RcaScriptStarter().start(script_id, on_created, on_started, on_exited, exception_handler)
