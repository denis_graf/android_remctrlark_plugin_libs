# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from remctrlark.event_handling.ARcaEvent import ARcaEvent


class RcaEvent(ARcaEvent):
    def __init__(self, sl4a_event):
        """
        @type sl4a_event: dict[str, dict[str, object]]
        """
        super(RcaEvent, self).__init__()
        self._name = sl4a_event["name"]
        self._data = sl4a_event["data"]

    @property
    def name(self):
        """
        @rtype: str
        """
        return self._name

    @property
    def data(self):
        """
        @rtype: dict[str, object]
        """
        return self._data

    def __str__(self):
        return "{name: '%s', data: %s}" % (self.name, str(self.data))
