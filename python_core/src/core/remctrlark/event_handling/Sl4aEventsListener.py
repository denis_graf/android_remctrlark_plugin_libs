# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from threading import Thread
import threading


class Sl4aEventsListener(object):
    def __init__(self, proxy, event_handler, exception_handler=None):
        """
        @type proxy: ARcaProxyWrapper
        @type event_handler: ( (str, dict[str, object]) -> None ) | ( (object, str, dict[str, object]) -> None )
        @type exception_handler: ( () -> object ) | ( (object) -> object ) | None
        """
        super(Sl4aEventsListener, self).__init__()
        self._proxy = proxy
        self._event_handler = event_handler
        self._exception_handler = exception_handler
        self._running = False
        self._thread = None
        """@type: Thread"""

    def start(self, current_thread=False):
        if self._running:
            return
        self._running = True

        if current_thread:
            self._main_loop()
        else:
            self._thread = Thread(target=self._main_loop)
            self._thread.start()

    def shutdown(self):
        if self._running:
            self._running = False
            self._proxy.event_interrupt_wait_for()
        if self._thread is not None and self._thread.ident != threading.current_thread().ident:
            if self._thread.is_alive():
                self._thread.join()
            self._thread = None
            """@type: Thread"""

    def _main_loop(self):
        # noinspection PyBroadException
        try:
            while self._running:
                event = self._proxy.event_wait_for()
                if event is None:
                    continue
                self._event_handler(event.name, event.data)
        except:
            if self._exception_handler is None:
                raise
            else:
                # noinspection PyCallingNonCallable
                self._exception_handler()
        finally:
            self.shutdown()
