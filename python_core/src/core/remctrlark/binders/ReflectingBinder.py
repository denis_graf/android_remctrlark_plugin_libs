# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

import inspect
import pydoc

from remctrlark import utils
import rcapy
from remctrlark.binders.ARcaBinderMethod import ARcaBinderMethod
from remctrlark.binders.AProxyBinder import ARcaBinder
from remctrlark.error_handling.RcaException import RcaException


class ReflectingBinder(ARcaBinder):
    class BinderMethod(ARcaBinderMethod):
        def __init__(self, method):
            """
            @type method: object
            """
            super(ReflectingBinder.BinderMethod, self).__init__()
            self._name = method.__name__
            self._doc = utils.stripbs(self._create_method_doc(method))

        def get_name(self):
            return self._name

        def get_doc(self):
            return self._doc

        @staticmethod
        def _create_method_doc(method):
            """
            @type method: object
            @rtype: str
            """
            return pydoc.text.document(method, method.__name__)

    def __init__(self, binder_impl):
        """
        @type binder_impl: rcapy.AReflectingBinderImpl | list | tuple
        """
        super(ReflectingBinder, self).__init__()
        binder_methods_list = self._get_binder_methods(binder_impl)
        self._binder_methods = dict()
        for method in binder_methods_list:
            self._binder_methods[method.__name__] = method

    def invoke_method(self, name, args):
        """
        @type name: str
        @type args: tuple
        @rtype: object
        """
        return self._binder_methods[name](*args)

    def get_methods(self):
        """
        @rtype: list[ARcaBinderMethod]
        """
        result = list()
        for method in self._binder_methods.values():
            result.append(ReflectingBinder.BinderMethod(method))
        return result

    def _get_binder_methods(self, binder_impl):
        """
        @type binder_impl: rcapy.AReflectingBinderImpl | list | tuple
        @rtype: list | tuple
        """
        if binder_impl is None:
            binder_methods = None
        elif isinstance(binder_impl, rcapy.AReflectingBinderImpl):
            binder_methods = self._get_methods_of_reflecting_binder_impl(binder_impl)
        elif isinstance(binder_impl, list) or isinstance(binder_impl, tuple):
            binder_methods = binder_impl
        else:
            raise RcaException("Invalid binder implementation!")

        return binder_methods

    @staticmethod
    def _get_methods_of_reflecting_binder_impl(obj):
        """
        @type obj: rcapy.AReflectingBinderImpl
        @rtype: list
        """
        methods = []
        for key in dir(obj):
            try:
                value = getattr(obj, key)
            except AttributeError:
                continue
            assert isinstance(key, str)
            if inspect.ismethod(value) and not key.startswith("_"):
                methods.append(value)
        return methods
