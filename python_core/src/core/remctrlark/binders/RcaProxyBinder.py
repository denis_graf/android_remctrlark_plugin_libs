# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from .AProxyBinder import AProxyBinder


class RcaProxyBinder(AProxyBinder):

    def __init__(self, proxy, log):
        """
        @type proxy: RcaProxy
        @type log: ARcaLog
        """
        super(RcaProxyBinder, self).__init__(proxy, log, self.__class__.__name__)

    def invoke_method(self, name, args):
        """
        @type name: str
        @type args: tuple
        @rtype: object
        """
        if __debug__:
            self._log_rpc_call(name, args)
        result = self._proxy.rca_call_method(name, args)
        if __debug__:
            self._log_rpc_result(name, args, result)
        return result

    def get_methods(self):
        # TODO: Issue #30: ipython interface -> should we do something about it? For now there is no public access to
        # instances of RcaProxyBinder, so we don't need to implement this...
        raise NotImplementedError
