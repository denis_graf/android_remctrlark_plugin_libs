# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from remctrlark.binders.ARcaBinder import ARcaBinder
from remctrlark.rca_logging.RcaLogWrapper import RcaLogWrapper


# noinspection PyAbstractClass
class AProxyBinder(ARcaBinder):
    def __init__(self, proxy, log, log_tag):
        """
        @type proxy: RcaProxy
        @type log: ARcaLog
        """
        super(AProxyBinder, self).__init__()
        self._proxy = proxy
        self._log = RcaLogWrapper(log, log_tag)

    def _log_rpc_call(self, name, args):
        if __debug__:
            self._log.d("RPC enter: %s%s" % (name, args))

    def _log_rpc_result(self, name, args, result):
        if __debug__:
            self._log.d("RPC returned: %s%s -> %s" % (name, args, str(result)))
