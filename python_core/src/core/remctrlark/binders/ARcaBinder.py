# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from abc import ABCMeta, abstractmethod


class ARcaBinder(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def invoke_method(self, name, args):
        """
        @type name: str
        @type args: tuple
        @rtype: object
        """

    @abstractmethod
    def get_methods(self):
        """ Issue #30: ipython interface -> needed for auto-completion
        @rtype: tuple[ARcaBinderMethod] | list[ARcaBinderMethod]
        """

    def __getattr__(self, name):
        """
        @type name: str
        """
        if name == 'trait_names' or name == '_getAttributeNames':
            # Issue #30: ipython interface -> needed for auto-completion
            def result():
                return self.get_methods()

            return result

        def method_call(*args):
            return self.invoke_method(name, args)
        return method_call
