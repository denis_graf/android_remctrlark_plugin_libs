# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from abc import ABCMeta

import rcapy
from .AProxyBinder import AProxyBinder


# noinspection PyPep8Naming,PyPep8,PyShadowingBuiltins
class Sl4aProxyBinder(AProxyBinder, rcapy.ASl4aStub):
    __metaclass__ = ABCMeta

    def __init__(self, proxy, log):
        """
        @type proxy: RcaProxy
        @type log: ARcaLog
        """
        super(Sl4aProxyBinder, self).__init__(proxy, log, self.__class__.__name__)

    def invoke_method(self, name, args):
        """
        @type name: str
        @type args: tuple
        @rtype: object
        """
        if __debug__:
            self._log_rpc_call(name, args)
        if name.startswith("_rca"):
            from remctrlark.error_handling.RcaException import RcaException

            raise RcaException("Illegal call to RCA's internal SL4A-interface.")
        result = self._proxy.sl4a_call_method(name, args)
        if __debug__:
            self._log_rpc_result(name, args, result)
        return result

    def get_methods(self):
        return rcapy.get_sl4a_methods()

    def _sl4a_rpc(self, method, *args):
        """
        @type method: str
        @type args: tuple
        @rtype: object
        """
        return self.invoke_method(method, args)
