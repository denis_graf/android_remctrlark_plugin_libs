# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

import inspect
import pydoc

from rca_debug import load_test_env
from remctrlark.remocos.RemoConRelauncher import RemoConRelauncher


load_test_env()

from remctrlark.application.RcaScriptStarter import RcaScriptStarter, start_script_in_new_thread
from remctrlark.rca_proxy_wrappers.RcaIPyQueryProxy import RcaIPyQueryProxy


class _Logger(object):
    def __init__(self):
        super(_Logger, self).__init__()
        import logging
        self.logging_level = logging.NOTSET

    @property
    def set_level_to_debug(self):
        from rca_debug import set_logging_level_to_debug

        self.logging_level = set_logging_level_to_debug()
        return "logger level set to DEBUG [%d]" % self.logging_level

    @property
    def set_level_to_info(self):
        from rca_debug import set_logging_level_to_info

        self.logging_level = set_logging_level_to_info()
        return "logger level set to INFO [%d]" % self.logging_level


logger = _Logger()


class RcaQuery(object):
    def __init__(self):
        super(RcaQuery, self).__init__()
        self._remocon = None
        self._scripts = None

    @property
    def remocon(self):
        self._run_garbage_collector()
        proxy = RcaIPyQueryProxy()
        return RemoCon(proxy, proxy.rca_get_remocon())

    @property
    def scripts(self):
        self._run_garbage_collector()
        proxy = RcaIPyQueryProxy()
        return Scripts(proxy)

    @property
    def proxies(self):
        self._run_garbage_collector()
        proxy = RcaIPyQueryProxy()
        return Proxies(proxy)

    @property
    def sl4a(self):
        self._run_garbage_collector()
        proxy = RcaIPyQueryProxy()
        return proxy.sl4a

    @staticmethod
    def _run_garbage_collector():
        import gc
        gc.collect()


query = RcaQuery()


class RemoCoInfo(object):
    def __init__(self, proxy, remoco_id):
        """
        @type proxy: RcaIPyQueryProxy
        @type remoco_id: str
        """
        super(RemoCoInfo, self).__init__()
        self._proxy = proxy
        self._remoco_id = remoco_id

    def __getattr__(self, item):
        info = self._proxy.rca_get_remoco_info(self._remoco_id)
        assert isinstance(info, dict)
        if item == 'trait_names' or item == '_getAttributeNames':
            def result():
                return info.keys()

            return result
        return info[item]


class RemoCoBinderMethod(object):
    def __init__(self, proxy, remoco_id, method_name):
        """
        @type proxy: RcaIPyQueryProxy
        @type method_name: str
        """
        super(RemoCoBinderMethod, self).__init__()
        self._proxy = proxy
        self._remoco_id = remoco_id
        self._method_name = method_name

    def __call__(self, *args):
        return self._proxy.rca_call_remoco_binder_method(self._remoco_id, self._method_name, args)

    @property
    def help(self):
        doc_string = self._proxy.rca_get_remoco_binder_method(self._remoco_id, self._method_name)
        try:
            pydoc.getpager()(doc_string)
        except ImportError, pydoc.ErrorDuringImport:
            print doc_string
        return None


class RemoCoBinder(object):
    def __init__(self, proxy, remoco_id):
        """
        @type proxy: RcaIPyQueryProxy
        @type remoco_id: str
        """
        super(RemoCoBinder, self).__init__()
        self._proxy = proxy
        self._remoco_id = remoco_id

    def __getattr__(self, item):
        if item == 'trait_names' or item == '_getAttributeNames':
            def result():
                return self._proxy.rca_get_remoco_binder_methods(self._remoco_id)

            return result
        return RemoCoBinderMethod(self._proxy, self._remoco_id, item)


class RemoCo(object):
    def __init__(self, proxy, remoco_id):
        """
        @type proxy: RcaIPyQueryProxy
        @type remoco_id: str
        """
        super(RemoCo, self).__init__()
        self._proxy = proxy
        self._remoco_id = remoco_id

    @property
    def info(self):
        return RemoCoInfo(self._proxy, self._remoco_id)

    @property
    def detailed_info(self):
        doc_string = self._proxy.rca_get_remoco_detailed_info(self._remoco_id)
        try:
            pydoc.getpager()(doc_string)
        except ImportError, pydoc.ErrorDuringImport:
            print doc_string
        return None

    # TODO: currently only remo-comps offer a bindable interface
    @property
    def binder(self):
        return RemoCoBinder(self._proxy, self._remoco_id)

    # TODO: this one should be only accessible from a remo-comp
    def dispatch_event(self, name, data):
        return self._proxy.rca_dispatch_event(self._remoco_id, name, data)

    @property
    def relaunch(self):
        self._proxy.rca_relaunch_remoco(self._remoco_id)
        return "sent a request to relaunch remoco '%s'" % self._remoco_id


class RemoComps(object):
    def __init__(self, proxy):
        """
        @type proxy: RcaIPyQueryProxy
        """
        super(RemoComps, self).__init__()
        self._proxy = proxy

    def __getattr__(self, item):
        if item == 'trait_names' or item == '_getAttributeNames':
            def result():
                return self._proxy.rca_get_remocomps()

            return result
        return RemoComp(self._proxy, item)


class RemoCon(RemoCo):
    def __init__(self, proxy, remocon_id):
        """
        @type proxy: RcaIPyQueryProxy
        @type remocon_id: str
        """
        super(RemoCon, self).__init__(proxy, remocon_id)
        self._remocomps = None

    @property
    def remocomps(self):
        if self._remocomps is None:
            self._remocomps = RemoComps(self._proxy)
        return self._remocomps

    @property
    def relauncher(self):
        def relauncher_commit(commit):
            """
            @type commit: dict[str, object]
            """
            with RcaIPyQueryProxy() as proxy:
                proxy.rca_remocon_relauncher_commit(commit)

        return RemoConRelauncher(relauncher_commit)


class RemoComp(RemoCo):
    def __init__(self, proxy, remocomp_id):
        """
        @type proxy: RcaIPyQueryProxy
        @type remocomp_id: str
        """
        super(RemoComp, self).__init__(proxy, remocomp_id)


class Scripts(object):
    def __init__(self, proxy):
        """
        @type proxy: RcaIPyQueryProxy
        """
        super(Scripts, self).__init__()
        self._proxy = proxy

    def __getattr__(self, item):
        if item == 'trait_names' or item == '_getAttributeNames':
            def result():
                return self._proxy.rca_get_scripts()

            return result

        return Script(self._proxy, item)


class ScriptInfo(object):
    def __init__(self, proxy, script_id):
        """
        @type proxy: RcaIPyQueryProxy
        @type script_id: str
        """
        super(ScriptInfo, self).__init__()
        self._proxy = proxy
        self._script_id = script_id

    def __getattr__(self, item):
        info = self._proxy.rca_get_script_info(self._script_id)
        assert isinstance(info, dict)
        if item == 'trait_names' or item == '_getAttributeNames':
            def result():
                return info.keys()

            return result
        return info[item]


class Script(object):
    _ex_script_id = ""
    _ex_script = None

    def __init__(self, proxy, script_id):
        """
        @type proxy: RcaIPyQueryProxy
        @type script_id: str
        """
        super(Script, self).__init__()
        self._proxy = proxy
        self._script_id = script_id

    @property
    def info(self):
        return ScriptInfo(self._proxy, self._script_id)

    @property
    def instance(self):
        if Script._ex_script is None:
            instance = ScriptExNotStartedBinder(self._proxy, self._script_id)
        elif Script._ex_script_id == self._script_id:
            instance = ExScriptBinder(Script._ex_script)
        else:
            instance = ScriptExStartedBinder(self._proxy, self._script_id)
        return instance


class ScriptExNotStartedBinder(object):
    def __init__(self, proxy, script_id):
        """
        @type proxy: RcaIPyQueryProxy
        @type script_id: str
        """
        super(ScriptExNotStartedBinder, self).__init__()
        self._proxy = proxy
        self._script_id = script_id

    @property
    def start_exclusively(self):
        def on_created(script):
            Script._ex_script_id = self._script_id
            Script._ex_script = script

        start_script_in_new_thread(self._script_id, on_created)
        return "starting script '%s' exclusively..." % self._script_id

    @property
    def start_as_subprocess(self):
        # noinspection PyUnusedLocal
        def on_created(script):
            from remctrlark.rca_logging.RcaGlobalLog import RcaGlobalLog

            RcaGlobalLog.set_level(logger.logging_level)

        RcaScriptStarter().start_as_subprocess(self._script_id, on_created)
        return "starting script '%s' as a subprocess..." % self._script_id

    @property
    def relaunch(self):
        self._proxy.rca_script_relaunch(self._script_id)
        return "sent a request to relaunch script '%s'" % self._script_id


class ScriptExStartedBinder(object):
    def __init__(self, proxy, script_id):
        """
        @type proxy: RcaIPyQueryProxy
        @type script_id: str
        """
        super(ScriptExStartedBinder, self).__init__()
        self._proxy = proxy
        self._script_id = script_id

    @property
    def relaunch(self):
        self._proxy.rca_script_relaunch(self._script_id)
        return "sent a request to relaunch script '%s'" % self._script_id


class ExScriptBinder(object):
    def __init__(self, script):
        """
        @type script: ARcaScript
        """
        super(ExScriptBinder, self).__init__()
        self._script = script

    def __getattr__(self, item):
        if item == 'trait_names' or item == '_getAttributeNames':
            def result():
                attrs = _get_attrs(self._script)
                attrs.remove("start")
                return attrs

            return result
        return getattr(self._script, item)


def _get_attrs(obj):
    """
    @type obj: object
    @rtype: list
    """
    attrs = []
    for key in dir(obj):
        try:
            value = getattr(obj, key)
        except AttributeError:
            continue
        assert isinstance(key, str)
        if not key.startswith("_") and not inspect.isclass(value):
            attrs.append(key)
    return attrs


class Proxies(object):
    def __init__(self, proxy):
        """
        @type proxy: RcaIPyQueryProxy
        """
        super(Proxies, self).__init__()
        self._proxy = proxy

    def __getattr__(self, item):
        if item == 'trait_names' or item == '_getAttributeNames':
            def result():
                return self._proxy.rca_get_proxies()

            return result

        # TODO: Issue #30: ipython interface: implement proxy interface
        # return Proxy(self._proxy, item)
        return "no interface implemented for proxy yet"
