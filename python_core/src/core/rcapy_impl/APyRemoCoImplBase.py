# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

import rcapy


# noinspection PyAbstractClass
class APyRemoCoImplBase(rcapy.APyRemoCoImpl):
    """
    This abstract base class is to be implemented in order to provide a Python RemoCo type. It provides a minimalistic
    base implementation and some helper methods.
    """

    PARAM_NAME_DEBUG = "DEBUG"
    """
    RemoCo parameter identifier used by the RemoCo's debugging setting.
    """

    def __init__(self, context):
        """
        The instantiation of a RemoCo implementation is performed when the RemoCo instance is either created for the
        first time or when it is recreated during a relaunch. The instantiation is done by calling the RemoCo
        implementation's standard constructor followed by a call of this method.
        During instantiation of a RemoCo implementation only the most necessary initializations have to be done, the
        less the better. The developer should keep in mind, that after a RemoCo has instantiated its implementation and
        created the UI, the implementation instance still can be shut down by the RemoCon, without having started it.
        @param context: The RemoCo's context.
        @type context: rcapy.APyRemoCoContext
        """
        self.__rca_context = context
        self.__config = context.config
        self.__log = context.log
        self.__layout = context.layout
        self.__parent_view = context.layout.parent_view

        self.debug = False
        """
        True if the debugging setting is activated in the RemoCo's configuration.
        This member variable is intended to be used by developers of RemoCos as a switch for activating debugging code,
        e.g., additional logging and run-time checks.
        """
        self._init_debug()

    @property
    def context(self):
        """
        Returns the RemoCo's context instance.
        @rtype: rcapy.APyRemoCoContext
        """
        return self.__rca_context

    @property
    def config(self):
        """
        @see rcapy.APyRemoCoContext.config
        @rtype: rcapy.APyRemoCoConfig
        """
        return self.__config

    @property
    def log(self):
        """
        @see rcapy.APyRemoCoContext.log
        @rtype: rcapy.ARemoCoLog
        """
        return self.__log

    @property
    def layout(self):
        """
        @see rcapy.APyRemoCoContext.layout
        @rtype: rcapy.APyRemoCoLayout
        """
        return self.__layout

    @property
    def parent_view(self):
        """
        @see rcapy.APyRemoCoContext.layout.parent_view
        @rtype: rcapy.ASl4aGroupView
        """
        return self.__parent_view

    def shutdown(self):
        """
        @rtype: None
        """

    def create_ui(self):
        """
        @rtype: None
        """
        self.context.layout.create_main()

    def _init_debug(self):
        """
        Sets the debugging setting of this RemoCo by reading its configuration.
        @rtype: None
        """
        self.debug = self.config.get_param(APyRemoCoImplBase.PARAM_NAME_DEBUG, self.debug)
