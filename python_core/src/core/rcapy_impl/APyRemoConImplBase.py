# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from rcapy_impl.APyRemoCoImplBase import APyRemoCoImplBase
import rcapy


# noinspection PyAbstractClass
class APyRemoConImplBase(APyRemoCoImplBase, rcapy.APyRemoConImpl):
    """
    This abstract base class is to be implemented in order to provide a Python RemoCon type. It provides a minimalistic
    base implementation and some helper methods.
    """

    def __init__(self, context):
        """
        @type context: rcapy.APyRemoConContext
        """
        super(APyRemoConImplBase, self).__init__(context)

    @property
    def context(self):
        """
        Returns the RemoCon's context instance.
        @rtype: rcapy.APyRemoConContext
        """
        return super(APyRemoConImplBase, self).context

    @property
    def config(self):
        """
        @see rcapy.APyRemoCoContext.config
        @rtype: rcapy.APyRemoConConfig
        """
        return super(APyRemoConImplBase, self).config

    @property
    def layout(self):
        """
        @rtype: rcapy.APyRemoConLayout
        """
        return super(APyRemoConImplBase, self).layout

    @property
    def relauncher(self):
        """
        @see rcapy.APyRemoConContext.relauncher
        @rtype: rcapy.ARemoConRelauncher
        """
        return self.context.relauncher

    def start(self, configuration):
        """
        @type configuration: APyRemoConConfiguration
        """
