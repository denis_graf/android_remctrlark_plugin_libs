# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from rcapy_impl.APyRemoCoImplBase import APyRemoCoImplBase
import rcapy


# noinspection PyAbstractClass
class APyRemoCompImplBase(APyRemoCoImplBase, rcapy.APyRemoCompImpl):
    """
    This abstract base class is to be implemented in order to provide a Python RemoComp type. It provides a minimalistic
    base implementation and some helper methods.
    """

    def __init__(self, context):
        """
        @type context: rcapy.APyRemoCompContext
        """
        super(APyRemoCompImplBase, self).__init__(context)

    @property
    def context(self):
        """
        Returns the RemoComp's context instance.
        @rtype: rcapy.APyRemoCompContext
        """
        return super(APyRemoCompImplBase, self).context

    @property
    def config(self):
        """
        @rtype: rcapy.APyRemoCompConfig
        """
        return super(APyRemoCompImplBase, self).config

    @property
    def layout(self):
        """
        @rtype: rcapy.APyRemoCompLayout
        """
        return super(APyRemoCompImplBase, self).layout

    @property
    def event_dispatcher(self):
        """
        @see rcapy.APyRemoCompContext.event_dispatcher
        @rtype: rcapy.APyRemoCompEventDispatcher
        """
        return self.context.event_dispatcher

    def create_binder(self):
        """
        @rtype: APyRemoCompBinderImpl | list | tuple
        """

    def start(self, configuration):
        """
        @type configuration: APyRemoCompConfiguration
        """
