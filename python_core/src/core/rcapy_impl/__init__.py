# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from .APyRemoCompImplBase import APyRemoCompImplBase
from .APyRemoConImplBase import APyRemoConImplBase

TOUCH_ACTION_DOWN = 0
"""@type: int"""
TOUCH_ACTION_UP = 1
"""@type: int"""
TOUCH_ACTION_CANCEL = 3
"""@type: int"""

KEY_ACTION_DOWN = 0
"""@type: int"""
KEY_ACTION_UP = 1
"""@type: int"""
