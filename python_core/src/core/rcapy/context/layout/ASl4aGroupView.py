# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from abc import ABCMeta, abstractmethod


class ASl4aGroupView(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def query(self):
        """ Get layout Properties.
        @rtype: dict[str, dict[str, str]]
        """

    @abstractmethod
    def query_detail(self, view_id):
        """ Get layout properties for a specific widget.
        @param view_id: id of layout widget
        @type view_id: str
        @rtype: dict[str, str]
        """

    @abstractmethod
    def get_property(self, view_id, view_property):
        """ Get layout widget property or call a getter of the Java class instance.
        @param view_id: id of layout widget
        @param view_property: name of property to set
        @type view_id: str
        @type view_property: str
        @rtype object
        """

    @abstractmethod
    def set_property(self, view_id, view_property, value):
        """ Set layout widget property or call a setter of the Java class instance.
        @param view_id: id of layout widget
        @param view_property: name of property to set
        @param value: value to set property to
        @type view_id: str
        @type view_property: str
        @type value: object
        """

    @abstractmethod
    def set_list(self, view_id, values_list):
        """ Attach a list to a layout widget.
        @param view_id: id of layout widget
        @param values_list: List to set
        @type view_id: str
        @type values_list: str
        """

    @abstractmethod
    def set_on_custom_event_listener(self, view_id, listener):
        """
        @type view_id: str
        @type listener: None |
                        ( (str, dict[str, object]) -> None ) |
                        ( (object, str, dict[str, object]) -> None )
        """

    @abstractmethod
    def set_on_click_listener(self, view_id, listener):
        """
        @type view_id: str
        @type listener: None |
                        ( (str, dict[str, object]) -> None ) |
                        ( (object, str, dict[str, object]) -> None )
        """

    @abstractmethod
    def set_on_item_click_listener(self, view_id, listener):
        """
        @type view_id: str
        @type listener: None |
                        ( (str, dict[str, object]) -> None ) |
                        ( (object, str, dict[str, object]) -> None )
        """

    @abstractmethod
    def set_on_touch_listener(self, actions, view_id, listener):
        """
        @param actions: Combination of following actions is supported:
                            rcapy_impl.TOUCH_ACTION_DOWN = 0
                            rcapy_impl.TOUCH_ACTION_UP = 1
                            rcapy_impl.TOUCH_ACTION_CANCEL = 3
        @type actions: list[int]
        @type view_id: str
        @type listener: None |
                        ( (str, dict[str, object]) -> None ) |
                        ( (object, str, dict[str, object]) -> None )
        """

    @abstractmethod
    def call_method(self, view_id, method_name, args):
        """ Call a method of the Java class instance.
        @param view_id: id of layout widget
        @param method_name: name of property to set
        @param args: actual parameters
        @type view_id: str
        @type method_name: str
        @type args: tuple
        @rtype object | None
        """

    @abstractmethod
    def call_method_on_ui_thread(self, view_id, method_name, args):
        """ Call a method of the Java class instance on UI thread.
        @param view_id: id of layout widget
        @param method_name: name of property to set
        @param args: actual parameters
        @type view_id: str
        @type method_name: str
        @type args: tuple
        @rtype object | None
        """
