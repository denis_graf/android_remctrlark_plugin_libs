# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from abc import abstractmethod

from rcapy.context.layout.APyRemoCoLayout import APyRemoCoLayout


# noinspection PyAbstractClass
class APyRemoConLayout(APyRemoCoLayout):
    @abstractmethod
    def set_on_key_listener(self, actions, key_code, listener):
        """
        @param actions: Combination of following actions is supported:
                            rcapy_impl.KEY_ACTION_DOWN = 0
                            rcapy_impl.KEY_ACTION_UP = 1
        @type actions: list[int]
        @type key_code: int
        @type listener: None |
                        ( (str, dict[str, object]) -> None ) |
                        ( (object, str, dict[str, object]) -> None )
        """
