# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from abc import ABCMeta, abstractproperty


class APyRemoCoLayout(object):
    __metaclass__ = ABCMeta

    @abstractproperty
    def create_main(self):
        pass

    @abstractproperty
    def create_from_xml_string(self, xml):
        """
        @type xml: str
        """
        pass

    # noinspection PyPropertyDefinition
    @abstractproperty
    @property
    def parent_view(self):
        """
        @rtype: ASl4aGroupView
        """
