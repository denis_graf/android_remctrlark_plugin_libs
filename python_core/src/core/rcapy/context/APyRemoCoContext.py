# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from abc import ABCMeta, abstractproperty, abstractmethod


# noinspection PyPropertyDefinition
class APyRemoCoContext(object):
    """
    This interface describes the RCA context interface common to Python RemoComps and Python RemoCons.
    When a RemoCo implementation is instantiated by a RemoCo instance, it gets the RCA context via the constructor
    which provides access to the RemoCo instance and the RCA Framework.
    """

    __metaclass__ = ABCMeta

    @property
    @abstractproperty
    def the_id(self):
        """
        Returns the RemoCo's ID.
        @rtype: str
        """

    @property
    @abstractproperty
    def the_type(self):
        """
        Returns the RemoCo's current type.
        @rtype: str
        """

    @property
    @abstractproperty
    def the_apk(self):
        """
        Returns the APK's package name of the current RemoCo type's implementation.
        @rtype: str
        """

    @abstractproperty
    @property
    def namespace(self):
        """
        Returns RemoCo's ROS namespace.
        @rtype: str
        """

    @abstractproperty
    @property
    def config(self):
        """
        Returns the RemoCo's interface for retrieving its configuration.
        @rtype: APyRemoCoConfig
        """

    @abstractproperty
    @property
    def layout(self):
        """
        Returns the RemoCo's interface for accessing its layout (the user interface).
        @rtype: APyRemoCoLayout
        """

    @property
    @abstractproperty
    def log(self):
        """
        Returns the RemoCo's logger.
        @rtype: ARemoCoLog
        """

    @abstractmethod
    def handle_exception(self):
        """
        This method can be used for letting exceptions be handled by the RemoCo's exception handling mechanism.
        The same mechanism is used internally by RemoCos for handling uncaught exceptions.
        @rtype: None
        """

    @abstractmethod
    def post_to_impl_thread(self, runnable):
        """
        Posts the runnable to the event queue of the RemoCo implementation's thread for being executed checked.
        @param runnable: Runnable to be executed checked.
        @return: True, if the runnable could be successfully posted; False otherwise.
        @type runnable: ( () -> None ) | ( (object) -> None )
        @rtype: bool
        """

    @abstractmethod
    def run_on_impl_thread(self, runnable):
        """
        Executes checked the runnable on the RemoCo implementation's thread.
        If the current thread is the RemoCo implementation's thread, the runnable is executed immediately.
        Otherwise, it is posted to the event queue of the RemoCo implementation's thread.
        @param runnable: Runnable to be executed checked.
        @return: True, if the runnable could be successfully posted or executed; False otherwise.
        @type runnable: runnable: ( () -> None ) | ( (object) -> None )
        @rtype: bool
        """

    @abstractmethod
    def sync_run_on_impl_thread(self, runnable):
        """
        Executes checked the runnable on the RemoCo implementation's thread.
        If the current thread is the RemoCo implementation's thread, the runnable is executed immediately.
        Otherwise, this method posts the runnable to the event queue of the RemoCo implementation's thread, and waits
        for it to return.
        @param runnable: Runnable to be executed checked.
        @return: True, if the runnable could be successfully executed; False otherwise.
        @type runnable: runnable: ( () -> None ) | ( (object) -> None )
        @rtype: bool
        """

    @abstractmethod
    def start_loading_task(self, msg):
        """
        Causes the Main App to block the RemoCon's UI by showing a progress bar with the specified message.
        It returns an instance of an interface representing the loading task, which provides a method to be called after
        the task is done.
        @param msg: The message to be shown while the loading task is performed.
        @return: Interface representing the started loading task.
        @type msg: str
        @rtype: ARemoCoLoadingTask | None
        """

    @abstractmethod
    def create_android_proxy(self):
        """
        Creates a new instance of RCA's AAndroidProxy for providing access to Android's API via SL4A.
        @rtype: AAndroidProxy
        """

    @abstractmethod
    def request_remocon_reboot_on_relaunch(self, enable):
        """
        If the RemoCo's implementation leaks memory, this method can be used in certain cases as a workaround.
        If enable is set to true, then a relaunch of this RemoCo forces the RemoCon to "reboot".
        Rebooting a RemoCon means reconnecting the Main App to the ROS Master, and relaunching the RemoCon.
        Per default this feature is disabled.
        @type enable: bool
        @rtype: None
        """

    @abstractmethod
    def is_on_shutdown(self):
        """
        Returns True, if the RemoCo is currently shutting down. Otherwise, it returns False.
        @rtype: bool
        """

    @abstractmethod
    def is_on_relaunch(self):
        """
        Returns true, if the RemoCo is currently relaunching. Otherwise, it returns false.
        @rtype: bool
        """

    @abstractmethod
    def set_remocon_session_state(self, state):
        """
        This method can be used for implementing a persistent state, which "survives" after a RemoCo is relaunched.
        @param state: The object describing the RemoCo's RemoCon Session State which can be retrieved with
        getRemoConSessionState().
        @type state: object | None
        @rtype: None
        """

    @abstractmethod
    def get_remocon_session_state(self):
        """
        This method can be used for retrieving the RemoCo's persistent state.
        @return The object describing the RemoCo's RemoCon Session State which was previously set with
        setRemoConSessionState().
        @rtype: object | None
        """
