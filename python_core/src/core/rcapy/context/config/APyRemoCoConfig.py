# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from abc import ABCMeta, abstractmethod


class APyRemoCoConfig(object):
    __metaclass__ = ABCMeta

    class _Unspecified(object):
        pass

    _unspecified = _Unspecified()

    @abstractmethod
    def get_params(self, namespace=None):
        """
        @type namespace: str | None
        @rtype: dict[str, object]
        """

    @abstractmethod
    def get_param(self, name, default_value=_unspecified):
        """
        @type name: str
        @type default_value: str | int | float | bool
        @rtype: str | int | float | bool
        @raise RemoCoParamNotFoundException: If parameter could not be found and default_value is not specified.
        """
