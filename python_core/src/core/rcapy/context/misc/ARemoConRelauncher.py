# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from abc import ABCMeta, abstractmethod


class ARemoConRelauncher(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def connect_to_master(self, uri):
        """
        @type uri: str | None
        @rtype: ARemoConRelauncher
        """

    @abstractmethod
    def connect_to_new_public_master(self):
        """
        @rtype: ARemoConRelauncher
        """

    @abstractmethod
    def connect_to_new_private_master(self):
        """
        @rtype: ARemoConRelauncher
        """

    @abstractmethod
    def set_namespace(self, nsid, namespace):
        """
        @type nsid: str
        @type namespace: str
        @rtype: ARemoConRelauncher
        """

    @abstractmethod
    def set_namespace(self, nsid, namespace):
        """
        @type nsid: str
        @type namespace: str
        @rtype: ARemoConRelauncher
        """

    @abstractmethod
    def set_type(self, tid, remoco_type, apk=None):
        """
        @type tid: str
        @type remoco_type: str
        @type apk: str | None
        @rtype: ARemoConRelauncher
        """

    @abstractmethod
    def set_param(self, name, value):
        """
        @type name: str
        @type value: str | int | float | bool
        @rtype: ARemoConRelauncher
        """

    @abstractmethod
    def set_remocomp_param(self, remocomp_id, name, value):
        """
        @type remocomp_id: str
        @type name: str
        @type value: str | int | float | bool
        @rtype: ARemoConRelauncher
        """

    @abstractmethod
    def set_remap(self, remap_from, remap_to, nsid=None):
        """
        @type remap_from: str
        @type remap_to: str
        @type nsid: str | None
        @rtype: ARemoConRelauncher
        """

    @abstractmethod
    def set_remocomp_remap(self, remocomp_id, remap_from, remap_to, nsid=None):
        """
        @type remocomp_id: str
        @type remap_from: str
        @type remap_to: str
        @type nsid: str | None
        @rtype: ARemoConRelauncher
        """

    @abstractmethod
    def set_all_to_initial_state(self):
        """
        @rtype: ARemoConRelauncher
        """

    @abstractmethod
    def commit(self):
        """
        @rtype: None
        """
