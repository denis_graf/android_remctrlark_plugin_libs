# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from abc import ABCMeta, abstractmethod, abstractproperty


# noinspection PyPropertyDefinition
class ARemoCompProxy(object):
    __metaclass__ = ABCMeta

    @property
    @abstractproperty
    def the_type(self):
        """
        @rtype: str
        """

    @property
    @abstractproperty
    def the_apk(self):
        """
        @rtype: str
        """

    @property
    @abstractproperty
    def the_id(self):
        """
        @rtype: str
        """

    @property
    @abstractproperty
    def binder(self):
        """
        @rtype: ARemoCompBinder
        """

    @abstractmethod
    def set_events_listener(self, listener):
        """
        @type listener: None |
                        ( (rcapy.ARemoCompProxy, str, dict[str, object]) -> None ) |
                        ( (object, rcapy.ARemoCompProxy, str, dict[str, object]) -> None )
        """

    @abstractmethod
    def set_on_launched_listener(self, listener):
        """
        @type listener: None |
                        ( (rcapy.ARemoCompProxy) -> None ) |
                        ( (object, rcapy.ARemoCompProxy -> None )
        """
