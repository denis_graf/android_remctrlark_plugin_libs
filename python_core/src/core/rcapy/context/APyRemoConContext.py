# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from abc import abstractmethod, abstractproperty

from rcapy import APyRemoCoContext


# noinspection PyAbstractClass,PyPropertyDefinition
class APyRemoConContext(APyRemoCoContext):
    """
    This interface describes the RCA context interface of Python RemoCons.
    When a RemoCo implementation is instantiated by a RemoCo instance, it gets the RCA context via the constructor
    which provides access to the RemoCo instance and the RCA Framework.
    """

    @abstractproperty
    @property
    def config(self):
        """
        @rtype: APyRemoConConfig
        """

    @abstractproperty
    @property
    def layout(self):
        """
        @rtype: APyRemoConLayout
        """

    @abstractmethod
    def get_remocomp_proxy(self, remocomp_id):
        """
        Retrieves the proxy of a running RemoComp with the specified ID for accessing basic information about the
        RemoComp and using the communication routines.
        @param remocomp_id: RemoComp ID.
        @return: The proxy of the RemoComp.
        @type remocomp_id: str
        @rtype: ARemoCompProxy
        """

    @abstractproperty
    @property
    def relauncher(self):
        """
        Returns the RemoCon's relauncher interface for relaunching RemoCos by changing its configuration dynamically.
        @rtype: ARemoConRelauncher
        """
