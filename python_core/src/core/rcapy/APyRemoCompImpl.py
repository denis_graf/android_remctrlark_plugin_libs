# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from abc import abstractmethod

from .APyRemoCoImpl import APyRemoCoImpl


# noinspection PyAbstractClass
class APyRemoCompImpl(APyRemoCoImpl):
    """
    This interface is to be implemented in order to provide a Python RemoComp type. Please use the abstract base class
    rcapy_impl.APyRemoCompImplBase.
    """

    @abstractmethod
    def __init__(self, context):
        """
        The instantiation of a RemoCo implementation is performed when the RemoCo instance is either created for the
        first time or when it is recreated during a relaunch. The instantiation is done by calling this constructor.
        During instantiation of a RemoCo implementation only the most necessary initializations have to be done, the
        less the better. The developer should keep in mind, that after a RemoCo has instantiated its implementation and
        created the UI, the implementation instance still can be shut down by the RemoCon, without having started it.
        @param context: The RemoComp's context.
        @type context: APyRemoCompContext
        """

    @abstractmethod
    def create_binder(self):
        """
        This method creates the RemoComp's Binder Interface. It is called during the instantiation of the RemoComp.
        @return: RemoComp's Binder Interface.
        @rtype: APyRemoCompBinderImpl | list | tuple
        """

    @abstractmethod
    def start(self, configuration):
        """
        This method is called when the RemoComp gets started.
        It is executed when all appropriate requirements are given, i.e., all RemoCos and their UIs are created
        and the ROS Master URI has been checked to be reachable. First all RemoComps are started by calling their
        implementations of this method. After all RemoComps have been started, the RemoCon's implementation of this
        method is called.
        @param configuration Contains the information needed for using ROS.
        @type configuration: APyRemoCompConfiguration
        @rtype: None
        """
