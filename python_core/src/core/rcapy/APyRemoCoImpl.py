# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from abc import ABCMeta, abstractmethod


class APyRemoCoImpl(object):
    """
    This interface is to be implemented in order to provide a Python RemoCo type. Please use the abstract base class
    rcapy_impl.APyRemoCoImplBase.
    """

    __metaclass__ = ABCMeta

    @abstractmethod
    def shutdown(self):
        """
        This method is called when the RemoCo gets shut down, either when the Main App gets destroyed, or when the
        RemoCo gets relaunched by the RCA Relauncher.
        The implementation of a RemoCo's shutdown() method must properly free all resources which have been acquired by
        the RemoCo implementation's instance during its lifetime. After a RemoCo implementation instance has been shut
        down, it won't be used anymore by RCA and is intended to be garbage collected.
        Note: If not properly implemented, the Main App can leak memory when using the RCA Relauncher.
        @rtype: None
        """

    @abstractmethod
    def create_ui(self):
        """
        This method is intended to create the RemoCo's UI.
        For placing RemoComps in the RemoCon, one has to use RemoComp Anchors (defined by rca.RemoComp tags) in the
        RemoCon's XML layout. After the RemoCon's implementation of this method method has returned, the RemoComps are
        created for each RemoComp Anchor, and the RemoComps' implementations of this method are called.
        @rtype: None
        """
