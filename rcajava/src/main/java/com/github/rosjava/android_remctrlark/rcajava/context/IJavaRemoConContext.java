/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.rcajava.context;

import com.github.rosjava.android_remctrlark.rcajava.context.config.IJavaRemoConConfig;
import com.github.rosjava.android_remctrlark.rcajava.context.layout.IJavaRemoConLayout;
import com.github.rosjava.android_remctrlark.rcajava.context.misc.IRemoCompProxy;
import com.github.rosjava.android_remctrlark.rcajava.context.misc.IRemoConRelauncher;

/**
 * This interface describes the RCA context interface of Java RemoComps.
 * When a RemoCo implementation is instantiated by a RemoCo instance, it gets the RCA context via the callback method
 * {@link com.github.rosjava.android_remctrlark.rcajava.IJavaRemoConImpl#init(IJavaRemoConContext)} which provides
 * access to the RemoCo instance and the RCA Framework.
 */
public interface IJavaRemoConContext extends IJavaRemoCoContext {

	@Override
	IJavaRemoConConfig getConfig();

	@Override
	IJavaRemoConLayout getLayout();

	/**
	 * Retrieves the proxy of a running RemoComp with the specified ID for accessing basic information about the
	 * RemoComp and using the communication routines.
	 * @param id RemoComp ID.
	 * @return The proxy of the RemoComp.
	 */
	IRemoCompProxy getRemoCompProxy(String id);

	/**
	 * Returns the RemoCon's relauncher interface for relaunching RemoCos by changing its configuration dynamically.
	 */
	IRemoConRelauncher getRelauncher();

}
