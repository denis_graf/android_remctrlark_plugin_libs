/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.rcajava.context.misc;

import org.ros.namespace.GraphName;

import java.net.URI;

public interface IRemoConRelauncher {

	IRemoConRelauncher connectToMaster(String uri);

	IRemoConRelauncher connectToMaster(URI uri);

	IRemoConRelauncher connectToNewPublicMaster();

	IRemoConRelauncher connectToNewPrivateMaster();


	IRemoConRelauncher setNamespace(String nsId, String namespace);

	IRemoConRelauncher setNamespace(String nsId, GraphName namespace);


	IRemoConRelauncher setType(String tId, String type);

	IRemoConRelauncher setType(String tId, String type, String apk);


	IRemoConRelauncher setParam(String name, double value);

	IRemoConRelauncher setParam(GraphName name, double value);

	IRemoConRelauncher setParam(String name, int value);

	IRemoConRelauncher setParam(GraphName name, int value);

	IRemoConRelauncher setParam(String name, boolean value);

	IRemoConRelauncher setParam(GraphName name, boolean value);

	IRemoConRelauncher setParam(String name, String value);

	IRemoConRelauncher setParam(GraphName name, String value);


	IRemoConRelauncher setRemoCompParam(String id, String name, double value);

	IRemoConRelauncher setRemoCompParam(String id, GraphName name, double value);

	IRemoConRelauncher setRemoCompParam(String id, String name, int value);

	IRemoConRelauncher setRemoCompParam(String id, GraphName name, int value);

	IRemoConRelauncher setRemoCompParam(String id, String name, boolean value);

	IRemoConRelauncher setRemoCompParam(String id, GraphName name, boolean value);

	IRemoConRelauncher setRemoCompParam(String id, String name, String value);

	IRemoConRelauncher setRemoCompParam(String id, GraphName name, String value);


	IRemoConRelauncher setRemap(String from, String to);

	IRemoConRelauncher setRemap(GraphName from, GraphName to);

	IRemoConRelauncher setRemap(String from, String to, String nsId);

	IRemoConRelauncher setRemap(GraphName from, GraphName to, String nsId);


	IRemoConRelauncher setRemoCompRemap(String id, String from, String to);

	IRemoConRelauncher setRemoCompRemap(String id, GraphName from, GraphName to);

	IRemoConRelauncher setRemoCompRemap(String id, String from, String to, String nsId);

	IRemoConRelauncher setRemoCompRemap(String id, GraphName from, GraphName to, String nsId);


	IRemoConRelauncher setAllToInitialState();

	void commit();

}
