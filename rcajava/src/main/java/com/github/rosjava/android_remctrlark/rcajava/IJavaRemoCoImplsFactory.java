/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.rcajava;

import java.util.Set;

/**
 * This interface is to be implemented in order to provide a factory of Java RemoCo types. You can use the abstract base
 * class {@link com.github.rosjava.android_remctrlark.rcajava_impl.AJavaRemoCoImplsFactoryBase}.
 */
public interface IJavaRemoCoImplsFactory {

	/**
	 * Returns a set of all RemoCon type identifiers provided by this factory.
	 * @return Set of RemoCon type identifiers.
	 * @throws Throwable
	 */
	Set<String> getAllRemoConTypes() throws Throwable;

	/**
	 * Returns a set of all RemoComp type identifiers provided by this factory.
	 * @return Set of RemoComp type identifiers.
	 * @throws Throwable
	 */
	Set<String> getAllRemoCompTypes() throws Throwable;

	/**
	 * Creates an instace of a RemoCon type.
	 * @param type Identifier of the RemoCon type to be created.
	 * @return Created RemoCon type instance.
	 * @throws Throwable
	 */
	IJavaRemoConImpl createRemoConImpl(String type) throws Throwable;

	/**
	 * Creates an instace of a RemoComp type.
	 * @param type Identifier of the RemoComp type to be created.
	 * @return Created RemoComp type instance.
	 * @throws Throwable
	 */
	IJavaRemoCompImpl createRemoCompImpl(String type) throws Throwable;

}
