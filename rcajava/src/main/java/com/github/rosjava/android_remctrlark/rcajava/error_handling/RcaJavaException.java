/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.rcajava.error_handling;

public class RcaJavaException extends Exception {

	private final String mRemoCoId;

	/**
	 * Constructs a new {@code RcaJavaException} that includes the current stack
	 * trace.
	 *
	 * @param remoCoId id of the involved RemoCo.
	 */
	public RcaJavaException(String remoCoId) {
		mRemoCoId = remoCoId;
	}

	/**
	 * Constructs a new {@code RcaJavaException} with the current stack trace
	 * and the specified detail message.
	 *
	 * @param remoCoId id of the involved RemoCo.
	 * @param message the detail message for this exception.
	 */
	public RcaJavaException(String remoCoId, String message) {
		super(message);
		mRemoCoId = remoCoId;
	}

	/**
	 * Constructs a new {@code RcaJavaException} with the current stack trace,
	 * the specified detail message and the specified cause.
	 *
	 * @param remoCoId id of the involved RemoCo.
	 * @param message the detail message for this exception.
	 * @param cause     the cause of this exception.
	 */
	public RcaJavaException(String remoCoId, String message, Throwable cause) {
		super(message, cause);
		mRemoCoId = remoCoId;
	}

	/**
	 * Constructs a new {@code RcaJavaException} with the current stack trace
	 * and the specified cause.
	 *
	 * @param remoCoId id of the involved RemoCo.
	 * @param cause the cause of this exception.
	 */
	public RcaJavaException(String remoCoId, Throwable cause) {
		super(cause);
		mRemoCoId = remoCoId;
	}

	/**
	 * Return the id of the involved RemoCo.
	 *
	 * @return id of the involved RemoCo, or {@code null} if no id could determined.
	 */
	public String getRemoCoId() {
		return mRemoCoId;
	}
}
