/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.rcajava.context.misc;

public interface IRemoCoLog {
	/**
	 * Send a {@link android.util.Log#DEBUG} log message. Check {@link com.github.rosjava.android_remctrlark.BuildConfig#DEBUG} before calling this method:
	 * <pre>
	 * if (BuildConfig.DEBUG) {
	 *     Log.d("Some message");
	 * }
	 * </pre>
	 * @param msg The message you would like to be logged.
	 */
	void d(String msg);

	void i(String msg);

	void e(String msg);

	void w(String msg);
}
