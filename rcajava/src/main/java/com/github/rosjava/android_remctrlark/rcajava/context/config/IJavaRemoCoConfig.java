/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.rcajava.context.config;

import org.ros.namespace.GraphName;

import java.util.Map;

public interface IJavaRemoCoConfig {

	Map<GraphName, Object> getParams();

	Map<GraphName, Object> getParams(String namespace);

	Map<GraphName, Object> getParams(GraphName namespace);


	Object getParam(GraphName name);

	Object getParam(String name);

	Object getParam(GraphName name, Object defaultValue);

	Object getParam(String name, Object defaultValue);


	double getParamDouble(GraphName name);

	double getParamDouble(String name);

	double getParamDouble(GraphName name, double defaultValue);

	double getParamDouble(String name, double defaultValue);


	int getParamInteger(GraphName name);

	int getParamInteger(String name);

	int getParamInteger(GraphName name, int defaultValue);

	int getParamInteger(String name, int defaultValue);


	boolean getParamBoolean(GraphName name);

	boolean getParamBoolean(String name);

	boolean getParamBoolean(GraphName name, boolean defaultValue);

	boolean getParamBoolean(String name, boolean defaultValue);


	String getParamString(GraphName name);

	String getParamString(String name);

	String getParamString(GraphName name, String defaultValue);

	String getParamString(String name, String defaultValue);

}
