/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.rcajava.context;

import com.github.rosjava.android_remctrlark.rcajava.context.config.IJavaRemoCompConfig;
import com.github.rosjava.android_remctrlark.rcajava.context.layout.IJavaRemoCompLayout;
import com.github.rosjava.android_remctrlark.rcajava.context.misc.IJavaRemoCompEventDispatcher;

/**
 * This interface describes the RCA context interface of Java RemoComps.
 * When a RemoCo implementation is instantiated by a RemoCo instance, it gets the RCA context via the callback method
 * {@link com.github.rosjava.android_remctrlark.rcajava.IJavaRemoCompImpl#init(IJavaRemoCompContext)} which provides
 * access to the RemoCo instance and the RCA Framework.
 */
public interface IJavaRemoCompContext extends IJavaRemoCoContext {

	@Override
	IJavaRemoCompConfig getConfig();

	@Override
	IJavaRemoCompLayout getLayout();

	/**
	 * Retrieves the event dispatcher for dispatching events to the running RemoCon.
	 */
	IJavaRemoCompEventDispatcher getEventDispatcher();

}
