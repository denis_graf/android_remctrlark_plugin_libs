/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.rcajava.context;

import android.content.Context;

import com.github.rosjava.android_remctrlark.rcajava.context.config.IJavaRemoCoConfig;
import com.github.rosjava.android_remctrlark.rcajava.context.layout.IJavaRemoCoLayout;
import com.github.rosjava.android_remctrlark.rcajava.context.misc.IRemoCoLoadingTask;
import com.github.rosjava.android_remctrlark.rcajava.context.misc.IRemoCoLog;
import com.github.rosjava.android_remctrlark.rcajava.misc.ICheckedRunnable;

import org.ros.namespace.GraphName;
import org.ros.node.NodeMainExecutor;

/**
 * This interface describes the RCA context interface common to Java RemoComps and Java RemoCons.
 * When a RemoCo implementation is instantiated by a RemoCo instance, it gets the RCA context via the callback method
 * {@code init()} which provides access to the RemoCo instance and the RCA Framework.
 */
public interface IJavaRemoCoContext {

	/**
	 * Returns the RemoCo's ID.
	 */
	String getId();

	/**
	 * Returns the RemoCo's current type.
	 */
	String getType();

	/**
	 * Returns the APK's package name of the current RemoCo type's implementation.
	 */
	String getApk();

	/**
	 * Returns RemoCo's ROS namespace.
	 */
	GraphName getNamespace();

	/**
	 * Returns the RemoCo's interface for retrieving its configuration.
	 */
	IJavaRemoCoConfig getConfig();

	/**
	 * Returns the RemoCo's interface for accessing its layout (the user interface).
	 */
	IJavaRemoCoLayout getLayout();

	/**
	 * Returns the RemoCo's logger.
	 */
	IRemoCoLog getLog();

	/**
	 * This method can be used for letting exceptions be handled by the RemoCo's exception handling mechanism.
	 * The same mechanism is used internally by RemoCos for handling uncaught exceptions.
	 * @param e Exception to handle.
	 */
	void handleException(Throwable e);

	/**
	 * Posts the runnable to the event queue of the RemoCo implementation's thread for being executed checked.
	 * @param runnable Runnable to be executed checked.
	 * @return True, if the runnable could be successfully posted; false otherwise.
	 */
	boolean postToImplThread(ICheckedRunnable runnable);

	/**
	 * Executes checked the runnable on the RemoCo implementation's thread.
	 * If the current thread is the RemoCo implementation's thread, the runnable is executed immediately.
	 * Otherwise, it is posted to the event queue of the RemoCo implementation's thread.
	 * @param runnable Runnable to be executed checked.
	 * @return True, if the runnable could be successfully posted or executed; false otherwise.
	 */
	boolean runOnImplThread(ICheckedRunnable runnable);

	/**
	 * Executes checked the runnable on the RemoCo implementation's thread.
	 * If the current thread is the RemoCo implementation's thread, the runnable is executed immediately.
	 * Otherwise, this method posts the runnable to the event queue of the RemoCo implementation's thread, and waits for
	 * it to return.
	 * @param runnable Runnable to be executed checked.
	 * @return True, if the runnable could be successfully executed; false otherwise.
	 */
	boolean syncRunOnImplThread(ICheckedRunnable runnable);

	/**
	 * Posts the runnable to the event queue of the UI thread for being executed checked.
	 * @param runnable Runnable to be executed checked.
	 * @return True, if the runnable could be successfully posted; false otherwise.
	 */
	boolean postToUiThread(ICheckedRunnable runnable);

	/**
	 * Executes checked the runnable on the UI thread.
	 * If the current thread is the UI thread, the runnable is executed immediately.
	 * Otherwise, it is posted to the event queue of the UI thread.
	 * @param runnable Runnable to be executed checked.
	 * @return True, if the runnable could be successfully posted or executed; false otherwise.
	 */
	boolean runOnUiThread(ICheckedRunnable runnable);

	/**
	 * Executes checked the runnable on the UI thread.
	 * If the current thread is the UI thread, the runnable is executed immediately.
	 * Otherwise, this method posts the runnable to the event queue of the UI thread, and waits for it to return.
	 * @param runnable Runnable to be executed checked.
	 * @return True, if the runnable could be successfully executed; false otherwise.
	 */
	boolean syncRunOnUiThread(ICheckedRunnable runnable);

	/**
	 * Causes the Main App to block the RemoCon's UI by showing a progress bar with the specified message.
	 * It returns an instance of an interface representing the loading task, which provides a method to be called after
	 * the task is done.
	 * @param msg The message to be shown while the loading task is performed.
	 * @return Interface representing the started loading task.
	 */
	IRemoCoLoadingTask startLoadingTask(String msg);

	/**
	 * Returns the instance of ROSJava's {@link NodeMainExecutor} started by the Main App.
	 * It is intended to be used for executing ROS nodes.
	 */
	NodeMainExecutor getNodeExecutor();

	/**
	 * Returns an instance of Android's Context for the APK which includes the RemoCo implementation's source code and
	 * resources.
	 * It can be used for accessing resources provided by the plugin's APK.
	 */
	Context getApkContext();

	/**
	 * If the RemoCo's implementation leaks memory, this method can be used in certain cases as a workaround.
	 * If enable is set to true, then a relaunch of this RemoCo forces the RemoCon to "reboot".
	 * Rebooting a RemoCon means reconnecting the Main App to the ROS Master, and relaunching the RemoCon.
	 * Per default this feature is disabled.
	 */
	void requestRemoConRebootOnRelaunch(boolean enable);

	/**
	 * Returns true, if the RemoCo is currently shutting down. Otherwise, it returns false.
	 */
	boolean isOnShutdown();

	/**
	 * Returns true, if the RemoCo is currently relaunching. Otherwise, it returns false.
	 */
	boolean isOnRelaunch();

	/**
	 * This method can be used for implementing a persistent state, which "survives" after a RemoCo is relaunched.
	 * @param state The object describing the RemoCo's RemoCon Session State which can be retrieved with
	 * {@link #getRemoConSessionState()}.
	 */
	void setRemoConSessionState(Object state);

	/**
	 * This method can be used for retrieving the RemoCo's persistent state.
	 * @return The object describing the RemoCo's RemoCon Session State which was previously set with
	 * {@link #setRemoConSessionState(Object)}.
	 */
	Object getRemoConSessionState();

}
