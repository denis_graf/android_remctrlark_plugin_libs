/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.rcajava.error_handling;

public class RcaLayoutException extends RcaJavaException {

	public RcaLayoutException(String remoCoId, String message) {
		this(remoCoId, message, null);
	}

	public RcaLayoutException(String remoCoId, Throwable cause) {
		this(remoCoId, null, cause);
	}

	public RcaLayoutException(String remoCoId, String message, Throwable cause) {
		super(remoCoId, "Error on performing layout operation" + (message != null ? ": " + message : "."), cause);
	}

}
