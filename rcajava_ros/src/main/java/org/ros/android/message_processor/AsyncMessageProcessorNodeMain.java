/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package org.ros.android.message_processor;

import org.ros.message.MessageListener;
import org.ros.namespace.GraphName;
import org.ros.node.ConnectedNode;
import org.ros.node.Node;
import org.ros.node.topic.Subscriber;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class AsyncMessageProcessorNodeMain<T, S> extends ASimpleMessageProcessorNodeMain<S> {
	private static final int MAX_QUEUE_SIZE_DEFAULT = 2;

	/**
	 * Use -1 for infinite queue.
	 */
	protected final int mMaxQueueSize;

	protected final Queue<S> mMessageQueue = new ConcurrentLinkedQueue<S>();
	protected final Object mEvent = new Object();
	private boolean mRunning = false;

	private final Thread mThread = new Thread(new Runnable() {
		@Override
		public void run() {
			while (mRunning) {
				final S message;
				synchronized (mMessageQueue) {
					if (mMessageQueue.size() > 0) {
						message = mMessageQueue.poll();
					} else {
						message = null;
					}
				}

				processMessage(message);

				synchronized (mEvent) {
					if (mRunning) {
						try {
							mEvent.wait();
						} catch (InterruptedException e) {
							Thread.currentThread().interrupt();
							break;
						}
					}
				}
			}
		}
	});

	public AsyncMessageProcessorNodeMain() {
		this(MAX_QUEUE_SIZE_DEFAULT);
	}

	public AsyncMessageProcessorNodeMain(int maxQueueSize) {
		mMaxQueueSize = maxQueueSize;
	}

	public AsyncMessageProcessorNodeMain(String messageType, String topicName, GraphName defaultNodeName) {
		this(messageType, topicName, defaultNodeName, MAX_QUEUE_SIZE_DEFAULT);
	}

	public AsyncMessageProcessorNodeMain(String messageType, String topicName, GraphName defaultNodeName, int maxQueueSize) {
		super(messageType, topicName, defaultNodeName);
		mMaxQueueSize = maxQueueSize;
	}

	@Override
	public void onStart(ConnectedNode connectedNode) {
		super.onStart(connectedNode);
		mRunning = true;
		mThread.start();
		final Subscriber<S> subscriber = connectedNode.newSubscriber(getTopicName(), getMessageType());
		subscriber.addMessageListener(new MessageListener<S>() {
			@Override
			public void onNewMessage(S message) {
				mMessageQueue.add(message);
				if (mMaxQueueSize > -1 && mMessageQueue.size() > mMaxQueueSize) {
					mMessageQueue.remove();
				}
				synchronized (mEvent) {
					mEvent.notify();
				}
			}
		});
	}

	@Override
	public void onShutdownComplete(Node node) {
		if (mRunning) {
			synchronized (mEvent) {
				mRunning = false;
				mEvent.notify();
			}
			try {
				mThread.join();
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
			synchronized (mMessageQueue) {
				mMessageQueue.clear();
			}
		}
		super.onShutdownComplete(node);
	}
}
