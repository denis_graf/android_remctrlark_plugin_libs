/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package org.ros.android.message_processor;

import org.ros.namespace.GraphName;
import org.ros.node.ConnectedNode;
import org.ros.node.Node;

public abstract class ASimpleMessageProcessorNodeMain<T> implements MessageProcessorNodeMain<T> {
	private MessageProcessor<T> mMessageProcessor = null;
	private String mTopicName = null;
	private String mMessageType = null;
	private GraphName mDefaultNodeName = null;

	protected ASimpleMessageProcessorNodeMain() {
	}

	protected ASimpleMessageProcessorNodeMain(String messageType, String topicName, GraphName defaultNodeName) {
		setMessageType(messageType);
		setTopicName(topicName);
		setDefaultNodeName(defaultNodeName);
	}

	public MessageProcessor<T> getMessageProcessor() {
		return mMessageProcessor;
	}

	public void setMessageProcessor(MessageProcessor<T> messageProcessor) {
		mMessageProcessor = messageProcessor;
	}

	public String getTopicName() {
		return mTopicName;
	}

	public void setTopicName(String topicName) {
		mTopicName = topicName;
	}

	public String getMessageType() {
		return mMessageType;
	}

	public void setMessageType(String messageType) {
		mMessageType = messageType;
	}

	@Override
	public GraphName getDefaultNodeName() {
		return mDefaultNodeName;
	}

	public void setDefaultNodeName(GraphName defaultNodeName) {
		mDefaultNodeName = defaultNodeName;
	}

	@Override
	public void onStart(ConnectedNode node) {
	}

	@Override
	public void onShutdown(Node node) {
	}

	@Override
	public void onShutdownComplete(Node node) {
		setMessageProcessor(null);
	}

	@Override
	public void onError(Node node, Throwable throwable) {
	}

	protected void processMessage(T message) {
		final MessageProcessor<T> processor = getMessageProcessor();
		if (message != null && processor != null) {
			processor.processMessage(message);
		}
	}
}