/*
 * Copyright (C) 2011 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

/*
 * Copyright (C) 2014, Denis Graf. All rights reserved.
 *
 * Original RepeatingPublisher by damonkohler@google.com (Damon Kohler) modified by Denis Graf:
 * - Multi threading problem solved: it could happen that canceling a RepeatingPublisher immediately after starting it,
 *   throws an exception, because the working thread is not running yet.
 */

package org.ros.internal.node.topic;

import com.google.common.base.Preconditions;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ros.concurrent.CancellableLoop;
import org.ros.node.topic.Publisher;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Repeatedly send a message out on a given {@link org.ros.node.topic.Publisher}.
 * <p/>
 *
 * Original RepeatingPublisher by damonkohler@google.com (Damon Kohler) modified by Denis Graf:
 * - Multi threading problem solved: in prior version it could happen that canceling a RepeatingPublisher immediately
 * after starting it, throws an exception, because the working thread is not running yet.
 *
 * @author damonkohler@google.com (Damon Kohler)
 */

public class RcaRepeatingPublisher<MessageType> {

	private static final boolean DEBUG = false;
	private static final Log log = LogFactory.getLog(RcaRepeatingPublisher.class);

	private final Publisher<MessageType> publisher;
	private final MessageType message;
	private final int frequency;
	private final RepeatingPublisherLoop runnable;

	private CountDownLatch startedLatch = null;

	/**
	 * Executor used to run the {@link RepeatingPublisherLoop}.
	 */
	private final ScheduledExecutorService executorService;

	private final class RepeatingPublisherLoop extends CancellableLoop {
		@Override
		public void loop() throws InterruptedException {
			startedLatch.countDown();
			publisher.publish(message);
			if (DEBUG) {
				log.info(String.format("Published message %s to publisher %s ", message, publisher));
			}
			Thread.sleep((long) (1000.0d / frequency));
		}
	}

	/**
	 * @param frequency the frequency of publication in Hz
	 */
	public RcaRepeatingPublisher(Publisher<MessageType> publisher, MessageType message, int frequency,
	                             ScheduledExecutorService executorService) {
		this.publisher = publisher;
		this.message = message;
		this.frequency = frequency;
		this.executorService = executorService;
		runnable = new RepeatingPublisherLoop();
	}

	public void start() {
		Preconditions.checkState(startedLatch == null);
		startedLatch = new CountDownLatch(1);
		executorService.execute(runnable);
	}

	public void cancel() {
		Preconditions.checkState(startedLatch != null);
		try {
			startedLatch.await();
		} catch (InterruptedException e) {
			// Restore the interrupted status.
			Thread.currentThread().interrupt();
		}
		runnable.cancel();
	}
}
