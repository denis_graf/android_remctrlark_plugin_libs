/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package org.ros.internal.node;

import org.ros.namespace.GraphName;
import org.ros.node.ConnectedNode;
import org.ros.node.Node;
import org.ros.node.NodeMain;

import java.util.HashSet;
import java.util.Set;

public class NodeMainGroup implements NodeMain {
	private final Set<NodeMain> mNodeMains = new HashSet<NodeMain>();
	private GraphName mDefaultNodeName = null;

	public NodeMainGroup() {
	}

	public NodeMainGroup(GraphName defaultNodeName) {
		setDefaultNodeName(defaultNodeName);
	}

	public void addNodeMain(NodeMain nodeMain) {
		synchronized (mNodeMains) {
			mNodeMains.add(nodeMain);
		}
	}

	public boolean removeNodeMain(NodeMain nodeMain) {
		synchronized (mNodeMains) {
			return mNodeMains.remove(nodeMain);
		}
	}

	@Override
	public GraphName getDefaultNodeName() {
		return mDefaultNodeName;
	}

	public void setDefaultNodeName(GraphName defaultNodeName) {
		mDefaultNodeName = defaultNodeName;
	}

	@Override
	public void onStart(ConnectedNode node) {
		synchronized (mNodeMains) {
			for (NodeMain nodeMain : mNodeMains) {
				nodeMain.onStart(node);
			}
		}
	}

	@Override
	public void onShutdown(Node node) {
		synchronized (mNodeMains) {
			for (NodeMain nodeMain : mNodeMains) {
				nodeMain.onShutdown(node);
			}
		}
	}

	@Override
	public void onShutdownComplete(Node node) {
		synchronized (mNodeMains) {
			for (NodeMain nodeMain : mNodeMains) {
				nodeMain.onShutdownComplete(node);
			}
			mNodeMains.clear();
		}
	}

	@Override
	public void onError(Node node, Throwable throwable) {
		synchronized (mNodeMains) {
			for (NodeMain nodeMain : mNodeMains) {
				nodeMain.onError(node, throwable);
			}
		}
	}
}
