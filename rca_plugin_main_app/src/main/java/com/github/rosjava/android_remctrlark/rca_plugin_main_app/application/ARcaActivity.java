/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark.rca_plugin_main_app.application;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.github.rosjava.android_remctrlark.rca_plugin_main_app.RcaConstants;
import com.google.common.base.Preconditions;

/**
 * This abstract class has to be implemented by the RCA plugin. The path to the implementation of this class has to be
 * {@code .application.MainActivity} in the APK package.
 * TODO: #55: check installation of used plugins on remo-co startup -> implement:
 * The RCA main application looks for that Activity and starts it for installing the RCA plugin.
 */
public abstract class ARcaActivity extends Activity {

	public static final String EXTRA_ONLY_FOR_RCA_PLUGIN_INSTALL = "onlyForRcaPluginInstall";
	public static final String RCA_ACTIVITY = "com.github.rosjava.android_remctrlark.application.InitialChooserActivity";
	public static final String RCA_ACTIVITY_EXTRA_RCA_APK = "rcaApk";
	public static final String RCA_ACTIVITY_EXTRA_TITLE = "title";
	public static final String RCA_ACTIVITY_EXTRA_ONLY_FOR_RCA_PLUGIN_INSTALL = EXTRA_ONLY_FOR_RCA_PLUGIN_INSTALL;

	private static final int RCA_ACTIVITY_REQUEST_CODE = 2;
	private boolean mRcaActivityStarted = false;

	/**
	 * This method must be overridden. If {@link #isOnlyForRcaPluginInstall()} returns true, it must call
	 * {@link #startRcaActivity()} and finish the activity after installation is done.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	/**
	 * Returns true when this activity was started only for installation of this RCA plugin. This is the case when
	 * shared components of this RCA plugin are requested by other RCA applications.
	 */
	@SuppressWarnings("UnusedDeclaration")
	protected boolean isOnlyForRcaPluginInstall() {
		return getIntent().getExtras() != null && getIntent().getExtras().getBoolean(EXTRA_ONLY_FOR_RCA_PLUGIN_INSTALL);
	}

	/**
	 * Returns the request code for starting the RCA activity of the main application. You can override it, if you
	 * wish to use some other value than the default.
	 * @return Request code
	 */
	protected int getRcaActivityRequestCode() {
		return RCA_ACTIVITY_REQUEST_CODE;
	}

	@Override
	public void startActivityForResult(Intent intent, int requestCode) {
		Preconditions.checkArgument(requestCode != getRcaActivityRequestCode(), "The request code is already in use!");
		super.startActivityForResult(intent, requestCode);
	}

	/**
	 * Called after the RCA activity has finished. You must finish here this activity, if
	 * {@link #isOnlyForRcaPluginInstall()} returns true.
	 */
	protected abstract void onRcaActivityFinished();

	@SuppressWarnings("UnusedDeclaration")
	protected void startRcaActivity() {
		startRcaActivity(false);
	}

	/**
	 * Starts the RCA activity. Before calling this method, all custom installations for running this RCA plugin have to
	 * be installed. If {@link #isOnlyForRcaPluginInstall()} returns true, you have to call this method in your
	 * overridden {@link #onCreate(android.os.Bundle)} method and finish the activity in your implementation of
	 * {@link #onRcaActivityFinished()}.
	 *
	 * @param startOnlyForInstall If {@code true}, the RCA activity starts only for installation and then finishes
	 */
	@SuppressWarnings("UnusedDeclaration")
	protected void startRcaActivity(boolean startOnlyForInstall) {
		Preconditions.checkState(!mRcaActivityStarted, "RcaActivity already started!");

		Intent intent = createRcaActivityIntent(startOnlyForInstall, getTitle().toString(), getPackageName());
		try {
			super.startActivityForResult(intent, getRcaActivityRequestCode());
		} catch (Throwable e) {
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
			alertDialog.setTitle("Error");
			alertDialog.setMessage("Rem-Ctrl-Ark plugin activity could not be started.\nPlease check your installation of Rem-Ctrl-Ark main application.");
			alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialog) {
					onRcaActivityFinished();
				}
			});
			alertDialog.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					onRcaActivityFinished();
				}
			});
			alertDialog.show();
			return;
		}
		mRcaActivityStarted = true;
	}

	/**
	 * Creates a new {@link android.content.Intent} for starting the RCA activity.
	 * @param startOnlyForInstall If {@code true}, the RCA activity starts only for installation and then finishes
	 * @param title Title of the RCA activity
	 * @param apk Package name of the RCA plugin to start
	 * @return Created {@link android.content.Intent} instance
	 */
	protected final Intent createRcaActivityIntent(boolean startOnlyForInstall, String title, String apk) {
		Intent intent = new Intent();
		intent.setClassName(RcaConstants.MAIN_APK, RCA_ACTIVITY);
		intent.putExtra(RCA_ACTIVITY_EXTRA_RCA_APK, apk);
		intent.putExtra(RCA_ACTIVITY_EXTRA_TITLE, title);
		intent.putExtra(RCA_ACTIVITY_EXTRA_ONLY_FOR_RCA_PLUGIN_INSTALL, startOnlyForInstall || isOnlyForRcaPluginInstall());
		return intent;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == getRcaActivityRequestCode()) {
			if (mRcaActivityStarted) {
				onRcaActivityFinished();
			}
		}
	}
}
